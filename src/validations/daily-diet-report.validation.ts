import { ReportStatus } from '@prisma/client';
import Joi from 'joi';

const getDailyDietReports = {
  query: Joi.object().keys({
    dietId: Joi.number(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDailyDietReport = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDailyDietReport = {
  body: Joi.object().keys({
    score: Joi.number().integer().required().min(1).max(10),
    status: Joi.string()
      .valid(ReportStatus.COMPLETE, ReportStatus.INCOMPLETE, ReportStatus.OVERCOMPLETE)
      .required(),
    feedback: Joi.string().required(),
    dayId: Joi.number().required(),
    expertId: Joi.number().required()
  })
};

const updateDailyDietReport = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    score: Joi.number().integer().required().min(1).max(10),
    status: Joi.string()
      .valid(ReportStatus.COMPLETE, ReportStatus.INCOMPLETE, ReportStatus.OVERCOMPLETE)
      .required(),
    feedback: Joi.string().required()
  })
};

const deleteDailyDietReport = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const dailyDietReportStatistic = {
  params: Joi.object({
    score: Joi.number().integer().min(1).max(10),
    status: Joi.string().valid(
      ReportStatus.COMPLETE,
      ReportStatus.INCOMPLETE,
      ReportStatus.OVERCOMPLETE
    )
  })
};

export default {
  getDailyDietReports,
  getDailyDietReport,
  createDailyDietReport,
  updateDailyDietReport,
  deleteDailyDietReport,
  dailyDietReportStatistic
};
