import { ApplicationType } from '@prisma/client';
import Joi from 'joi';

export const createApplicationSchema = Joi.object({
  type: Joi.string()
    .valid(
      ApplicationType.REGISTER_EXPERT,
      ApplicationType.CONTACT_EXPERT,
      ApplicationType.CREATE_DIET,
      ApplicationType.UPDATE_DIET,
      ApplicationType.DEPOSIT,
      ApplicationType.WITHDRAW,
      ApplicationType.REPORT
    )
    .required(),
  image: Joi.string().required(),
  description: Joi.string().required(),
  approvedById: Joi.number().required(),
  sourceId: Joi.number(),
  dietCategoryId: Joi.number()
});

export const expertApplicationSchema = Joi.object({
  image: Joi.string().required(),
  description: Joi.string().required()
});

const getApplications = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getApplication = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createApplication = {
  body: Joi.object().keys({
    type: Joi.string()
      .valid(
        ApplicationType.REGISTER_EXPERT,
        ApplicationType.CONTACT_EXPERT,
        ApplicationType.CREATE_DIET,
        ApplicationType.UPDATE_DIET,
        ApplicationType.DEPOSIT,
        ApplicationType.WITHDRAW,
        ApplicationType.REPORT
      )
      .required(),
    image: Joi.string().required(),
    description: Joi.string().required(),
    approvedById: Joi.number().required(),
    sourceId: Joi.number(),
    initialWeight: Joi.number(),
    targetWeight: Joi.number(),
    dietCategoryId: Joi.number(),
    endDate: Joi.date()
  })
};

const updateApplication = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    image: Joi.string().required(),
    description: Joi.string().required(),
    sourceId: Joi.number(),
    initialWeight: Joi.number(),
    targetWeight: Joi.number(),
    dietCategoryId: Joi.number(),
    endDate: Joi.date()
  })
};

const updateApplicationNote = {
  body: Joi.object().keys({ note: Joi.string().required() })
};

const deleteApplication = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getApplications,
  getApplication,
  createApplication,
  updateApplication,
  deleteApplication,
  expertApplicationSchema,
  createApplicationSchema,
  updateApplicationNote
};
