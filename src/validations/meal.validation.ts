import { MealType } from '@prisma/client';
import Joi from 'joi';

const getMeals = {
  query: Joi.object().keys({
    dietId: Joi.number(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getMeal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createMeal = {
  body: Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
    time: Joi.date().required(),
    type: Joi.string()
      .valid(MealType.LUNCH, MealType.OTHER, MealType.SNACK, MealType.DINNER, MealType.BREAKFAST)
      .required(),
    foods: Joi.array()
      .items(
        Joi.object().keys({
          amount: Joi.number().required(),
          id: Joi.number().required(),
          unit: Joi.string().required()
        })
      )
      .required(),
    dayId: Joi.number().required()
  })
};

const updateMeal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
    time: Joi.date().required(),
    type: Joi.string()
      .valid(MealType.LUNCH, MealType.OTHER, MealType.SNACK, MealType.DINNER, MealType.BREAKFAST)
      .required(),
    foods: Joi.array()
      .items(
        Joi.object().keys({
          amount: Joi.number().required(),
          id: Joi.number().required(),
          unit: Joi.string().required()
        })
      )
      .required(),
    dayId: Joi.number().required()
  })
};

const deleteMeal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getMeals,
  getMeal,
  createMeal,
  updateMeal,
  deleteMeal
};
