import Joi from 'joi';

const getRatings = {
  query: Joi.object().keys({
    expertId: Joi.number().integer(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getRating = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createRating = {
  body: Joi.object().keys({
    expertId: Joi.number().required(),
    score: Joi.number().required().min(0).max(5),
    description: Joi.string()
  })
};

const updateRating = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    score: Joi.number().required().min(0).max(5),
    description: Joi.string()
  })
};

const deleteRating = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getRatings,
  getRating,
  createRating,
  updateRating,
  deleteRating
};
