import Joi from 'joi';

export const dietCategorySchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().required()
});

const getDietCategories = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDietCategory = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDietCategory = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    high: Joi.array().items(Joi.string()).required(),
    low: Joi.array().items(Joi.string()).required()
  })
};

const updateDietCategory = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    high: Joi.array().items(Joi.string()).required(),
    low: Joi.array().items(Joi.string()).required()
  })
};

const deleteDietCategory = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getDietCategories,
  getDietCategory,
  createDietCategory,
  updateDietCategory,
  deleteDietCategory
};
