import Joi from 'joi';

export const calRecordSchema = Joi.object({
  nutrient: Joi.string().required(),
  // TODO: research check decimal or int
  amount: Joi.number().required(),
  unit: Joi.string().required(),
  dayCalId: Joi.number().required()
});

const getCalRecords = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getCalRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createCalRecord = {
  body: Joi.object().keys({
    nutrient: Joi.string().required(),
    amount: Joi.number().required(),
    unit: Joi.string().required(),
    dayCalId: Joi.number().required(),
    description: Joi.string()
  })
};

const updateCalRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    nutrient: Joi.string().required(),
    amount: Joi.number().required(),
    unit: Joi.string().required(),
    description: Joi.string()
  })
};

const deleteCalRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getCalRecords,
  getCalRecord,
  createCalRecord,
  updateCalRecord,
  deleteCalRecord
};
