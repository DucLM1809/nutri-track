import Joi from 'joi';

const getUserMedicalConditions = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getUserMedicalCondition = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createUserMedicalCondition = {
  body: Joi.object().keys({
    medicalConditionIds: Joi.array().items(Joi.number()).required()
  })
};

const deleteUserMedicalCondition = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getUserMedicalConditions,
  getUserMedicalCondition,
  createUserMedicalCondition,
  deleteUserMedicalCondition
};
