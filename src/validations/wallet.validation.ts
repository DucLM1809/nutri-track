import Joi from 'joi';

const getWallet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createWallet = {
  params: Joi.object().keys({
    balance: Joi.number().required()
  })
};

export default {
  getWallet,
  createWallet
};
