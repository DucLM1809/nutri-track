import Joi from 'joi';

const getTransactions = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getTransaction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createTransaction = {
  body: Joi.object().keys({
    amount: Joi.number().required(),
    description: Joi.string().required()
  })
};

export default {
  getTransactions,
  getTransaction,
  createTransaction
};
