import Joi from 'joi';
import { password } from './custom.validation';
import { bmiRecordSchema } from './bmi.validation';
import { createExpertSchema, createUserSchema } from './user.validation';
import { expertApplicationSchema } from './application.validation';
import { DeviceType } from '@prisma/client';

const register = {
  body: Joi.object().keys({
    user: createUserSchema.required(),
    bmiRecord: bmiRecordSchema.required(),
    medicalConditionIds: Joi.array().items(Joi.number()).required(),
    dietRestrictionIds: Joi.array().items(Joi.number()).required()
  })
};

const registerExpert = {
  body: Joi.object().keys({
    user: createExpertSchema.required(),
    application: expertApplicationSchema.required()
  })
};
const login = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
    deviceToken: Joi.string(),
    deviceType: Joi.string().valid(DeviceType.ANDROID, DeviceType.IOS)
  })
};

const loginGoogle = {
  body: Joi.object().keys({
    idToken: Joi.string().required()
  })
};

const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required()
  })
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required()
  })
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required()
  })
};

const resetPassword = {
  body: Joi.object().keys({
    otp: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required().custom(password)
  })
};

export default {
  register,
  registerExpert,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  loginGoogle
};
