import Joi from 'joi';

const currentDate = new Date().toISOString();

export const dietSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().required(),
  startDate: Joi.date().iso().min(currentDate).required(),
  endDate: Joi.date().iso().min(Joi.ref('startDate')).required(),
  initialWeight: Joi.number().min(0).required(),
  targetWeight: Joi.number().min(0).required(),
  actualWeight: Joi.number().min(0).required(),
  isActive: Joi.boolean().required(),
  categoryId: Joi.number().required(),
  userId: Joi.number().required()
});

const getDiets = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDiet = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    startDate: Joi.date().iso().min(currentDate).required(),
    endDate: Joi.date().iso().min(Joi.ref('startDate')).required(),
    initialWeight: Joi.number().min(0).required(),
    targetWeight: Joi.number().min(0).required(),
    actualWeight: Joi.number().min(0).required(),
    isActive: Joi.boolean().required(),
    categoryId: Joi.number().required(),
    userId: Joi.number().required()
  })
};

const changeDietStatus = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    isActive: Joi.number().required()
  })
};

const updateDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    initialWeight: Joi.number().min(0).required(),
    targetWeight: Joi.number().min(0).required(),
    actualWeight: Joi.number().min(0).required(),
    startDate: Joi.date().iso().min(currentDate).required(),
    endDate: Joi.date().iso().min(Joi.ref('startDate')).required(),
    isActive: Joi.boolean().required(),
    categoryId: Joi.number().required()
  })
};

const userUpdateDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    actualWeight: Joi.number().min(0).required()
  })
};

const deleteDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getDiets,
  getDiet,
  createDiet,
  updateDiet,
  changeDietStatus,
  deleteDiet,
  userUpdateDiet
};
