import Joi from 'joi';

export const dailyCalSchema = Joi.object({
  date: Joi.date().required(),
  description: Joi.string()
});

const getDailyCals = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDailyCal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDailyCal = {
  body: Joi.object().keys({
    date: Joi.date().required(),
    description: Joi.string()
  })
};

const updateDailyCal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    date: Joi.date().required(),
    description: Joi.string()
  })
};

const deleteDailyCal = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getDailyCals,
  getDailyCal,
  createDailyCal,
  updateDailyCal,
  deleteDailyCal
};
