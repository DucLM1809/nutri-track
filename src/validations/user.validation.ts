import { AccountType, Gender, Role } from '@prisma/client';
import Joi from 'joi';
import { password } from './custom.validation';

export const createUserSchema = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().required().custom(password),
  name: Joi.string().required(),
  avatar: Joi.string().required(),
  role: Joi.string().valid(Role.USER, Role.EXPERT, Role.ADMIN),
  gender: Joi.string().valid(Gender.MALE, Gender.FEMALE, Gender.OTHER),
  accountType: Joi.string().valid(AccountType.ADVANCE, AccountType.BASIC, AccountType.GOLD),
  dob: Joi.date().required(),
  height: Joi.number().required(),
  startFreeTime: Joi.date(),
  endFreeTime: Joi.date(),
  freeTimes: Joi.array().items(
    Joi.object({
      startFreeTime: Joi.date().required(),
      endFreeTime: Joi.date().required()
    })
  )
});

export const createExpertSchema = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().required().custom(password),
  name: Joi.string().required(),
  avatar: Joi.string().required(),
  role: Joi.string().valid(Role.USER, Role.EXPERT, Role.ADMIN),
  gender: Joi.string().valid(Gender.MALE, Gender.FEMALE, Gender.OTHER),
  accountType: Joi.string().valid(AccountType.ADVANCE, AccountType.BASIC, AccountType.GOLD),
  dob: Joi.date().required(),
  height: Joi.number(),
  startFreeTime: Joi.date(),
  endFreeTime: Joi.date(),
  freeTimes: Joi.array().items(
    Joi.object({
      startFreeTime: Joi.date().required(),
      endFreeTime: Joi.date().required()
    })
  )
});

export const ExpertProfileSchema = Joi.object({
  certImage: Joi.string().required()
});

const createUser = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    avatar: Joi.string().required(),
    role: Joi.string().valid(Role.USER, Role.ADMIN),
    gender: Joi.string().valid(Gender.MALE, Gender.FEMALE, Gender.OTHER),
    accountType: Joi.string().valid(AccountType.GOLD, AccountType.BASIC, AccountType.ADVANCE),
    dob: Joi.date().required(),
    startFreeTime: Joi.date(),
    endFreeTime: Joi.date(),
    freeTimes: Joi.array().items(
      Joi.object({
        startFreeTime: Joi.date().required(),
        endFreeTime: Joi.date().required()
      })
    )
  })
};

const getUsers = {
  query: Joi.object().keys({
    name: Joi.string(),
    role: Joi.string(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getUser = {
  params: Joi.object().keys({
    userId: Joi.number().integer()
  })
};

const updateUser = {
  params: Joi.object().keys({
    userId: Joi.number().integer()
  }),
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      password: Joi.string().custom(password),
      name: Joi.string(),
      height: Joi.number(),
      startFreeTime: Joi.date(),
      endFreeTime: Joi.date(),
      avatar: Joi.string(),
      dob: Joi.date(),
      gender: Joi.string().valid(Gender.MALE, Gender.FEMALE, Gender.OTHER),
      medicalConditionIds: Joi.array().items(Joi.number().integer()),
      freeTimes: Joi.array().items(
        Joi.object({
          startFreeTime: Joi.date().required(),
          endFreeTime: Joi.date().required()
        })
      )
    })
    .min(1)
};

const updateAccountType = {
  params: Joi.object().keys({
    userId: Joi.number().integer()
  }),
  body: Joi.object().keys({
    accountType: Joi.string().valid(AccountType.GOLD, AccountType.BASIC, AccountType.ADVANCE)
  })
};

const deleteUser = {
  params: Joi.object().keys({
    userId: Joi.number().integer()
  })
};

export default {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  updateAccountType
};
