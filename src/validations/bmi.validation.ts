import Joi from 'joi';

export const bmiRecordSchema = Joi.object({
  weight: Joi.number().required(),
  date: Joi.date().required()
});

const getBmiRecords = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getBmiRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createBmiRecord = {
  body: Joi.object().keys({
    weight: Joi.number().required(),
    date: Joi.date().required()
  })
};

const updateBmiRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    weight: Joi.number().required(),
    date: Joi.date().required()
  })
};

const deleteBmiRecord = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getBmiRecords,
  getBmiRecord,
  createBmiRecord,
  updateBmiRecord,
  deleteBmiRecord
};
