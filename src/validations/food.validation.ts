import Joi from 'joi';

const getFoods = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getFood = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default { getFoods, getFood };
