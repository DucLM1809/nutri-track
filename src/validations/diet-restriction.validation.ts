import Joi from 'joi';

const getDietRestrictions = {
  query: Joi.object().keys({
    avoid: Joi.string(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDietRestriction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDietRestriction = {
  body: Joi.object().keys({
    high: Joi.string(),
    low: Joi.string(),
    avoid: Joi.string()
  })
};

const updateDietRestriction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    high: Joi.string(),
    low: Joi.string(),
    avoid: Joi.string()
  })
};

const deleteDietRestriction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getDietRestrictions,
  getDietRestriction,
  createDietRestriction,
  updateDietRestriction,
  deleteDietRestriction
};
