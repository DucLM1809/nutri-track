import Joi from 'joi';

const getDailyDiets = {
  query: Joi.object().keys({
    dietId: Joi.number(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getDailyDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createDailyDiet = {
  body: Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
    date: Joi.date().required(),
    dietId: Joi.number().required()
  })
};

const updateDailyDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    name: Joi.string(),
    description: Joi.string(),
    date: Joi.date().required(),
    dietId: Joi.number().required()
  })
};

const deleteDailyDiet = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getDailyDiets,
  getDailyDiet,
  createDailyDiet,
  updateDailyDiet,
  deleteDailyDiet
};
