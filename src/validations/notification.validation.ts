import Joi from 'joi';

export const notificationSchema = Joi.object({
  title: Joi.string().required(),
  content: Joi.string().required(),
  isRead: Joi.boolean(),
  readAt: Joi.date().iso(),
  userId: Joi.number()
});

const getNotifications = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getNotification = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createNotification = {
  title: Joi.string().required(),
  content: Joi.string().required(),
  isRead: Joi.boolean(),
  readAt: Joi.date().iso(),
  userId: Joi.number()
};

const createNotificationTest = {
  title: Joi.string().required(),
  deviceToken: Joi.string().required()
};

const updateNotification = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required(),
    isRead: Joi.boolean(),
    readAt: Joi.date().iso(),
    userId: Joi.number()
  })
};

const readNotification = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const deleteNotification = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getNotifications,
  getNotification,
  createNotification,
  updateNotification,
  deleteNotification,
  readNotification,
  createNotificationTest
};
