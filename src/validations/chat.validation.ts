import { ChatStatus, ChatType } from '@prisma/client';
import Joi from 'joi';
import { ChatEvent } from '../types/chat.interface';

const getChats = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    eventName: Joi.string().valid(ChatEvent.CHAT_HISTORY, ChatEvent.JOIN_ROOM),
    receiverId: Joi.number().required()
  })
};

const createChat = {
  body: Joi.object().keys({
    eventName: Joi.string().valid(ChatEvent.CHAT_HISTORY, ChatEvent.JOIN_ROOM),
    type: Joi.string().valid(ChatType.MESSAGE, ChatType.VOICE),
    status: Joi.string().valid(ChatStatus.PROGRESSING, ChatStatus.CLOSED),
    receiverId: Joi.number().integer().required()
  })
};

const updateChat = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    status: Joi.string().valid(ChatStatus.PROGRESSING, ChatStatus.CLOSED),
    type: Joi.string().valid(ChatType.MESSAGE, ChatType.VOICE)
  })
};

export default {
  getChats,
  createChat,
  updateChat
};
