import Joi from 'joi';

export const medicalConditionSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string(),
  high: Joi.string(),
  low: Joi.string(),
  avoid: Joi.string()
});

const getMedicalConditions = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getMedicalCondition = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createMedicalCondition = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string(),
    high: Joi.string(),
    low: Joi.string(),
    avoid: Joi.string()
  })
};

const updateMedicalCondition = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string(),
    high: Joi.string(),
    low: Joi.string(),
    avoid: Joi.string()
  })
};

const deleteMedicalCondition = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getMedicalConditions,
  getMedicalCondition,
  createMedicalCondition,
  updateMedicalCondition,
  deleteMedicalCondition
};
