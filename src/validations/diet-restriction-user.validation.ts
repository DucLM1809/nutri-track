import Joi from 'joi';

const getUserDietRestrictions = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getUserDietRestriction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

const createUserDietRestriction = {
  body: Joi.object().keys({
    dietRestrictionIds: Joi.array().items(Joi.number()).required()
  })
};

const deleteUserDietRestriction = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getUserDietRestrictions,
  getUserDietRestriction,
  createUserDietRestriction,
  deleteUserDietRestriction
};
