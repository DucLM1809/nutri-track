import Joi from 'joi';
import { ChatEvent } from '../types/chat.interface';

const getMessages = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    sortType: Joi.string().valid('asc', 'desc'),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
    chatId: Joi.number().integer().required(),
    eventName: Joi.string().valid(ChatEvent.GET_MESSAGES, ChatEvent.SEND_MESSAGE)
  })
};

const createMessage = {
  body: Joi.object().keys({
    content: Joi.string().required(),
    chatId: Joi.number().integer().required(),
    eventName: Joi.string().valid(ChatEvent.GET_MESSAGES, ChatEvent.SEND_MESSAGE)
  })
};

const updateMessage = {
  params: Joi.object().keys({
    id: Joi.number().required()
  }),
  body: Joi.object().keys({
    content: Joi.string().required()
  })
};

const deleteMessage = {
  params: Joi.object().keys({
    id: Joi.number().required()
  })
};

export default {
  getMessages,
  createMessage,
  updateMessage,
  deleteMessage
};
