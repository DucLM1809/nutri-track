import { ApplicationStatus } from '@prisma/client';

export function mapApplicationAction(action: ApplicationStatus) {
  switch (action) {
    case ApplicationStatus.APPROVED:
      return 'approved';
    case ApplicationStatus.REJECTED:
      return 'rejected';
    default:
      return '';
  }
}
