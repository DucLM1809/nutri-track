export function roundedToHalf(num: number) {
  const fractionalPart = num % 1;
  if (fractionalPart < 0.25) {
    return Math.floor(num); // Round down to the nearest integer
  } else if (fractionalPart >= 0.25 && fractionalPart < 0.75) {
    return Math.floor(num) + 0.5; // Round to the nearest 0.5
  } else {
    return Math.ceil(num); // Round up to the nearest integer
  }
}
