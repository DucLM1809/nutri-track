import { ApplicationStatus } from '@prisma/client';

export const mapApplicationStatus = (status: ApplicationStatus): string => {
  switch (status) {
    case ApplicationStatus.PENDING:
      return 'Pending';
    case ApplicationStatus.APPROVED:
      return 'Approved';
    case ApplicationStatus.REJECTED:
      return 'Rejected';
    default:
      return '';
  }
};
