import express from 'express';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { dailyDietReportValidation } from '../../validations';
import { dailyDietReportController } from '../../controllers';

const router = express.Router();

router.get(
  '/statistic',
  auth('getDailyDietReportsStatistic'),
  validate(dailyDietReportValidation.dailyDietReportStatistic),
  dailyDietReportController.dailyDietReportStatistic
);

router
  .route('/')
  .get(
    auth('manageDailyDietReports'),
    validate(dailyDietReportValidation.getDailyDietReports),
    dailyDietReportController.getDailyDietReports
  )
  .post(
    auth('manageDailyDietReports'),
    validate(dailyDietReportValidation.createDailyDietReport),
    dailyDietReportController.createDailyDietReport
  );

router
  .route('/:id')
  .get(
    auth('manageDailyDietReports'),
    validate(dailyDietReportValidation.getDailyDietReport),
    dailyDietReportController.getDailyDietReport
  )
  .put(
    auth('manageDailyDietReports'),
    validate(dailyDietReportValidation.updateDailyDietReport),
    dailyDietReportController.updateDailyDietReport
  )
  .delete(
    auth('manageDailyDietReports'),
    validate(dailyDietReportValidation.deleteDailyDietReport),
    dailyDietReportController.deleteDailyDietReport
  );

/**
 * @swagger
 * tags:
 *   name: DailyDietReports
 *   description: Daily diet reports management and retrieval
 */

/**
 * @swagger
 * /daily-diet-reports:
 *   post:
 *     summary: Create a daily diet reports
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateDailyDietReport'
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DailyDietReport'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get user's daily diet reports
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: dietId
 *         schema:
 *           type: number
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/DailyDietReport'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /daily-diet-reports/{id}:
 *   get:
 *     summary: Get a daily diet report
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DailyDietReport'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   put:
 *     summary: Update a daily diet report
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              $ref: '#/components/schemas/UpdateDailyDietReport'
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DailyDietReport'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *   delete:
 *     summary: Delete a daily diet report
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 * /daily-diet-reports/statistic:
 *   get:
 *     summary: Get daily diet reports statistic
 *     tags: [DailyDietReports]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: params
 *         name: score
 *         schema:
 *           type: number
 *       - in: params
 *         name: status
 *         schema:
 *            $ref: '#/components/enums/ReportStatusEnum'
 *       - in: params
 *         name: from
 *         schema:
 *           type: string
 *           format: date-time
 *       - in: params
 *         name: to
 *         schema:
 *           type: string
 *           format: date-time
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 totalScoreMatch:
 *                   type: integer
 *                   example: 1
 *                 totalStatusMatch:
 *                   type: integer
 *                   example: 10
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */
export default router;
