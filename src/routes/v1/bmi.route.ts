import express from 'express';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { bmiController } from '../../controllers';
import { bmiValidation } from '../../validations';

const router = express.Router();

router
  .route('/')
  .get(auth('manageBmiRecords'), validate(bmiValidation.getBmiRecords), bmiController.getBmiRecords)
  .post(
    auth('manageBmiRecords'),
    validate(bmiValidation.createBmiRecord),
    bmiController.createBmiRecord
  );

router
  .route('/:id')
  .get(auth('manageBmiRecords'), validate(bmiValidation.getBmiRecord), bmiController.getBmiRecord)
  .put(
    auth('manageBmiRecords'),
    validate(bmiValidation.updateBmiRecord),
    bmiController.updateBmiRecord
  )
  .delete(
    auth('manageBmiRecords'),
    validate(bmiValidation.deleteBmiRecord),
    bmiController.deleteBmiRecord
  );

/**
 * @swagger
 * tags:
 *   name: BmiRecords
 *   description: Bmi record management and retrieval
 */

/**
 * @swagger
 * /bmi-records:
 *   post:
 *     summary: Create a bmi record
 *     tags: [BmiRecords]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - weight
 *               - date
 *             properties:
 *               weight:
 *                 type: number
 *               date:
 *                 type: string
 *                 format: date-time
 *             example:
 *                 weight: 10
 *                 date: 2020-05-12T16:18:04.793Z
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/BmiRecord'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get all users' bmi records
 *     tags: [BmiRecords]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/BmiRecord'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /bmi-records/{id}:
 *   get:
 *     summary: Get a bmi-record
 *     tags: [BmiRecords]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/BmiRecord'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   put:
 *     summary: Update a bmi record
 *     tags: [BmiRecords]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               weight:
 *                 type: number
 *               date:
 *                 type: string
 *                 format: date-time
 *             example:
 *               weight: 10
 *               date: 2020-05-12T16:18:04.793Z
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/BmiRecord'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *   delete:
 *     summary: Delete a bmi record
 *     tags: [BmiRecords]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Bmi record id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

export default router;
