import express from 'express';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { dietRestrictionValidation } from '../../validations';
import { dietRestrictionController } from '../../controllers';

const router = express.Router();

router
  .route('/')
  .get(
    validate(dietRestrictionValidation.getDietRestrictions),
    dietRestrictionController.getDietRestrictions
  )
  .post(
    auth('manageDietRestrictions'),
    validate(dietRestrictionController.createDietRestriction),
    dietRestrictionController.createDietRestriction
  );

router
  .route('/:id')
  .get(
    validate(dietRestrictionValidation.getDietRestriction),
    dietRestrictionController.getDietRestriction
  )
  .put(
    auth('manageDietRestrictions'),
    validate(dietRestrictionValidation.updateDietRestriction),
    dietRestrictionController.updateDietRestriction
  )
  .delete(
    auth('manageDietRestrictions'),
    validate(dietRestrictionValidation.deleteDietRestriction),
    dietRestrictionController.deleteDietRestriction
  );

/**
 * @swagger
 * tags:
 *   name: DietRestrictions
 *   description: Diet restriction management and retrieval
 */

/**
 * @swagger
 * /diet-restrictions:
 *   post:
 *     summary: Create a diet restriction
 *     tags: [DietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - high
 *               - low
 *               - avoid
 *             properties:
 *               high:
 *                 type: string
 *               low:
 *                 type: string
 *               avoid:
 *                 type: string
 *             example:
 *                 high: test
 *                 low: test
 *                 avoid: test
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DietRestriction'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get all diet restrictions
 *     tags: [DietRestrictions]
 *     parameters:
 *       - in: query
 *         name: avoid
 *         schema:
 *           type: string
 *       - in: query
 *         name: sortby
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/DietRestriction'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /diet-restrictions/{id}:
 *   get:
 *     summary: Get a diet restriction
 *     tags: [DietRestrictions]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DietRestriction'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   put:
 *     summary: Update a diet restriction
 *     tags: [DietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               high:
 *                 type: string
 *               low:
 *                 type: string
 *               avoid:
 *                 type: string
 *             example:
 *               high: test
 *               low: test
 *               avoid: test
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DietRestriction'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *   delete:
 *     summary: Delete a diet restriction
 *     tags: [DietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

export default router;
