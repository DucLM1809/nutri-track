import express from 'express';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { dietRestrictionUserValidation } from '../../validations';
import dietRestrictionUserController from '../../controllers/diet-restriction-user.controller';

const router = express.Router();

router
  .route('/')
  .get(
    auth('mangeDietRestrictionUser'),
    validate(dietRestrictionUserValidation.getUserDietRestrictions),
    dietRestrictionUserController.getUserDietRestrictions
  )
  .post(
    auth('mangeDietRestrictionUser'),
    validate(dietRestrictionUserValidation.createUserDietRestriction),
    dietRestrictionUserController.createUserDietRestriction
  );

router
  .route('/:id')
  .get(
    auth('mangeDietRestrictionUser'),
    validate(dietRestrictionUserValidation.getUserDietRestriction),
    dietRestrictionUserController.getUserDietRestriction
  )
  .delete(
    auth('mangeDietRestrictionUser'),
    validate(dietRestrictionUserValidation.deleteUserDietRestriction),
    dietRestrictionUserController.deleteUserDietRestriction
  );

/**
 * @swagger
 * tags:
 *   name: UserDietRestrictions
 *   description: User's diet restrictions management and retrieval
 */

/**
 * @swagger
 * /user-diet-restrictions:
 *   post:
 *     summary: Create diet restrictions for user
 *     tags: [UserDietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - dietRestrictionIds
 *             properties:
 *               dietRestrictionIds:
 *                  type: array
 *                  items:
 *                    type: number
 *             example:
 *                 dietRestrictionIds: [1]
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DietRestrictionUserResponse'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get user's diet restrictions
 *     tags: [UserDietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/DietRestrictionUserResponse'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /user-diet-restrictions/{id}:
 *   get:
 *     summary: Get a user's diet restriction
 *     tags: [UserDietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DietRestrictionUserResponse'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a user's diet restriction
 *     tags: [UserDietRestrictions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

export default router;
