import express from 'express';
import authRoute from './auth.route';
import userRoute from './user.route';
import docsRoute from './docs.route';
import applicationRoute from './application.route';
import bmiRoute from './bmi.route';
import foodRoute from './food.route';
import fileRoute from './file.route';
import medicalConditionRoute from './medical-condition.route';
import medicalConditionUserRoute from './medical-condition-user.route';
import dietRestrictionRoute from './diet-restriction.route';
import dietRestrictionUserRoute from './diet-restriction-user.route';
import dietRoute from './diet.route';
import dietCategoryRoute from './diet-category.route';
import dailyDietRoute from './daily-diet.route';
import dailyDietReportRoute from './daily-diet-report.route';
import dailyCalRoute from './daily-cal.route';
import mealRoute from './meal.route';
import calRecordRoute from './cal-record.route';
import chatsRoute from './chat.route';
import messagesRoute from './message.route';
import walletRoute from './wallet.route';
import transactionsRoute from './transaction.route';
import ratingsRoute from './rating.route';
import notificationsRoute from './notification.route';
import config from '../../config/config';

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute
  },
  {
    path: '/users',
    route: userRoute
  },
  {
    path: '/applications',
    route: applicationRoute
  },
  {
    path: '/bmi-records',
    route: bmiRoute
  },
  {
    path: '/medical-conditions',
    route: medicalConditionRoute
  },
  {
    path: '/diet-restrictions',
    route: dietRestrictionRoute
  },
  {
    path: '/user-diet-restrictions',
    route: dietRestrictionUserRoute
  },
  {
    path: '/foods',
    route: foodRoute
  },
  {
    path: '/file',
    route: fileRoute
  },
  {
    path: '/user-medical-conditions',
    route: medicalConditionUserRoute
  },
  {
    path: '/diets',
    route: dietRoute
  },
  {
    path: '/diet-categories',
    route: dietCategoryRoute
  },
  {
    path: '/daily-diets',
    route: dailyDietRoute
  },
  {
    path: '/daily-diet-reports',
    route: dailyDietReportRoute
  },
  {
    path: '/daily-cals',
    route: dailyCalRoute
  },
  { path: '/meals', route: mealRoute },
  { path: '/cal-records', route: calRecordRoute },
  { path: '/ratings', route: ratingsRoute },
  {
    path: '/chats',
    route: chatsRoute
  },
  {
    path: '/messages',
    route: messagesRoute
  },
  { path: '/wallet', route: walletRoute },
  { path: '/transactions', route: transactionsRoute },
  { path: '/notifications', route: notificationsRoute },
  {
    path: '/docs',
    route: docsRoute
  }
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute
  }
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

export default router;
