import express from 'express';
import { medicalConditionController } from '../../controllers';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { medicalConditionValidation } from '../../validations';

const router = express.Router();

router
  .route('/')
  .get(
    validate(medicalConditionValidation.getMedicalConditions),
    medicalConditionController.getMedicalConditions
  )
  .post(
    auth('manageMedicalConditions'),
    validate(medicalConditionValidation.createMedicalCondition),
    medicalConditionController.createMedicalCondition
  );

router
  .route('/:id')
  .get(
    validate(medicalConditionValidation.getMedicalCondition),
    medicalConditionController.getMedicalCondition
  )
  .put(
    auth('manageMedicalConditions'),
    validate(medicalConditionValidation.updateMedicalCondition),
    medicalConditionController.updateMedicalCondition
  )
  .delete(
    auth('manageMedicalConditions'),
    validate(medicalConditionValidation.deleteMedicalCondition),
    medicalConditionController.deleteMedicalCondition
  );

/**
 * @swagger
 * tags:
 *   name: MedicalConditions
 *   description: Medical conditions management and retrieval
 */

/**
 * @swagger
 * /medical-conditions:
 *   post:
 *     summary: Create a medical condition
 *     tags: [MedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *               - high
 *               - low
 *               - avoid
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *               high:
 *                 type: string
 *               low:
 *                 type: string
 *               avoid:
 *                 type: string
 *             example:
 *                 name: test
 *                 description: test
 *                 high: test
 *                 low: test
 *                 avoid: test
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MedicalCondition'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get all medical conditions
 *     tags: [MedicalConditions]
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: medical condition name
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/MedicalCondition'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /medical-conditions/{id}:
 *   get:
 *     summary: Get a medical condition
 *     tags: [MedicalConditions]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MedicalCondition'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   put:
 *     summary: Update a medical condition
 *     tags: [MedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *               high:
 *                 type: string
 *               low:
 *                 type: string
 *               avoid:
 *                 type: string
 *             example:
 *               name: test
 *               description: test
 *               high: test
 *               low: test
 *               avoid: test
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MedicalCondition'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *   delete:
 *     summary: Delete a medical condition
 *     tags: [MedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

export default router;
