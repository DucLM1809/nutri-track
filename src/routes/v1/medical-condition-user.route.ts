import express from 'express';
import { medicalConditionUserController } from '../../controllers';
import auth from '../../middlewares/auth';
import validate from '../../middlewares/validate';
import { medicalConditionUserValidation } from '../../validations';

const router = express.Router();

router
  .route('/')
  .get(
    auth('manageMedicalConditionUser'),
    validate(medicalConditionUserValidation.getUserMedicalConditions),
    medicalConditionUserController.getUserMedicalConditions
  )
  .post(
    auth('manageMedicalConditionUser'),
    validate(medicalConditionUserValidation.createUserMedicalCondition),
    medicalConditionUserController.createUserMedicalCondition
  );

router
  .route('/:id')
  .get(
    auth('manageMedicalConditionUser'),
    validate(medicalConditionUserValidation.getUserMedicalCondition),
    medicalConditionUserController.getUserMedicalCondition
  )
  .delete(
    auth('manageMedicalConditionUser'),
    validate(medicalConditionUserValidation.deleteUserMedicalCondition),
    medicalConditionUserController.deleteUserMedicalCondition
  );

/**
 * @swagger
 * tags:
 *   name: UserMedicalConditions
 *   description: User's medical conditions management and retrieval
 */

/**
 * @swagger
 * /user-medical-conditions:
 *   post:
 *     summary: Create a medical condition
 *     tags: [UserMedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - medicalConditionIds
 *             properties:
 *               medicalConditionIds:
 *                  type: array
 *                  items:
 *                    type: number
 *             example:
 *                 medicalConditionIds: [1]
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MedicalConditionUserResponse'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *   get:
 *     summary: Get user's medical conditions
 *     tags: [UserMedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/MedicalConditionUserResponse'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /user-medical-conditions/{id}:
 *   get:
 *     summary: Get a user's medical condition
 *     tags: [UserMedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: sortType
 *         schema:
 *            $ref: '#/components/enums/SortTypeEnum'
 *         default: asc
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MedicalConditionUserResponse'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a user's medical condition
 *     tags: [UserMedicalConditions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */

export default router;
