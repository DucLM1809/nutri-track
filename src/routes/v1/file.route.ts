import express from 'express';
import multer from 'multer';
import { fileController } from '../../controllers';

const router = express.Router();

// Setting up multer as a middleware to grab photo uploads
const upload = multer({ storage: multer.memoryStorage() });

router.route('/').post(upload.single('file'), fileController.uploadFile);

/**
 * @swagger
 * tags:
 *   name: File
 *   description: Upload file to firebase storage
 */

/**
 * @swagger
 * /file:
 *   post:
 *     summary: Upload file to firebase storage
 *     tags: [File]
 *     requestBody:
 *       required: true
 *       content:
 *        multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               file:
 *                 type: string
 *                 format: binary
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *               fileUrl:
 *                type: string
 *                format: uri
 *                example: https://firebasestorage.googleapis.com/v0/b/healthcare-1d3e9.appspot.com/o/2021-08-01T14%3A00%3A00.000Z-IMG_20210801_140000.jpg?alt=media&token=3e3e3e3e-3e3e-3e3e-3e3e-3e3e3e3e3e3e
 *       "400":
 *         $ref: '#/components/responses/BadRequest'
 */
export default router;
