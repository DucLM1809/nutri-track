import express from 'express';
import auth from '../../middlewares/auth';
import { walletController } from '../../controllers';

const router = express.Router();

router.route('/').get(auth('getWallet'), walletController.getUserWallet);
router.route('/').put(auth('updateWallet'), walletController.updateWallet);

/**
 * @swagger
 * tags:
 *   name: Wallets
 *   description: Wallet management and retrieval
 */

/**
 * @swagger
 * /wallet:
 *   get:
 *     summary: Get user's wallet
 *     tags: [Wallets]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Wallet'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /wallet:
 *   put:
 *     summary: Update user's wallet balance
 *     tags: [Wallets]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - amount
 *             properties:
 *               amount:
 *                 type: number
 *             example:
 *               amout: 100000
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                properties:
 *                  amount:
 *                    type: number
 *                example:
 *                  amount: 100
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

export default router;
