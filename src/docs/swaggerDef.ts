import { name, version, repository } from '../../package.json';
import config from '../config/config';

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: `${name} API documentation`,
    version,
    license: {
      name: 'MIT',
      url: repository
    }
  },
  servers: [
    {
      url: `${config.serverUrl}:${config.port}/v1`,
      description: 'Development server'
    },
    {
      url: `http://localhost:${config.port}/v1`,
      description: 'Local server'
    }
  ]
};

export default swaggerDef;
