import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { ReportStatus, Role, User } from '@prisma/client';
import { dailyDietReportService } from '../services';

const getDailyDietReports = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;

  const filter = pick(req.query, ['dietId']) as { dietId: number };
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  let dailyDietReports;

  if (role === Role.ADMIN) {
    dailyDietReports = await dailyDietReportService.getAllDailyDietReports(filter, options);
  } else {
    dailyDietReports = await dailyDietReportService.getDailyDietReports(id, filter, options);
  }

  res.status(httpStatus.OK).send(dailyDietReports);
});

const getDailyDietReport = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietReport = await dailyDietReportService.getDailyDietReport(Number(id));
  res.status(httpStatus.OK).send(dietReport);
});

const createDailyDietReport = catchAsync(async (req, res) => {
  const dailyDietReport = req.body;
  const createdDailyDiet = await dailyDietReportService.createDailyDietReport({
    ...dailyDietReport
  });
  res.status(httpStatus.CREATED).send(createdDailyDiet);
});

const updateDailyDietReport = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dailyDiet = req.body;
  const updatedDiet = await dailyDietReportService.updateDailyDietReport(Number(id), dailyDiet);
  res.status(httpStatus.OK).send(updatedDiet);
});

const deleteDailyDietReport = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dailyDietReportService.deleteDailyDietReport(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

const dailyDietReportStatistic = catchAsync(async (req, res) => {
  const { score, status, from, to } = req.params;

  const statisticResult = await dailyDietReportService.dailyDietReportStatistic(
    score,
    status,
    from,
    to
  );

  res.send(statisticResult);
});

export default {
  getDailyDietReport,
  getDailyDietReports,
  createDailyDietReport,
  updateDailyDietReport,
  deleteDailyDietReport,
  dailyDietReportStatistic
};
