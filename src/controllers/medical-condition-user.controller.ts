import { User } from '@prisma/client';
import httpStatus from 'http-status';
import medicalConditionUserService from '../services/medical-condition-user.service';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getUserMedicalConditions = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const medicalConditions = await medicalConditionUserService.getUserMedicalConditions(id, options);
  res.status(httpStatus.OK).send(medicalConditions);
});

const getUserMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.params;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const medicalCondition = await medicalConditionUserService.getMedicalConditionOfUser(id, options);
  res.status(httpStatus.OK).send(medicalCondition);
});

const createUserMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const { medicalConditionIds } = req.body;
  const createdMedicalCondition = await medicalConditionUserService.createUserMedicalCondition(
    id,
    medicalConditionIds
  );
  res.status(httpStatus.CREATED).send(createdMedicalCondition);
});

const deleteUserMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.params;
  await medicalConditionUserService.deleteUserMedicalCondition(id);
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getUserMedicalConditions,
  getUserMedicalCondition,
  createUserMedicalCondition,
  deleteUserMedicalCondition
};
