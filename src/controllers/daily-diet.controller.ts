import httpStatus from 'http-status';
import { dailyDietService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getDailyDiets = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dietId']);
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const dailyDiets = await dailyDietService.getDailyDiets(filter, options);

  res.status(httpStatus.OK).send(dailyDiets);
});

const getDailyDiet = catchAsync(async (req, res) => {
  const { id } = req.params;
  const diet = await dailyDietService.getDailyDiet(Number(id));
  res.status(httpStatus.OK).send(diet);
});

const createDailyDiet = catchAsync(async (req, res) => {
  const dailyDiet = req.body;
  const createdDailyDiet = await dailyDietService.createDailyDiet({
    ...dailyDiet
  });
  res.status(httpStatus.CREATED).send(createdDailyDiet);
});

const updateDailyDiet = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dailyDiet = req.body;
  const updatedDiet = await dailyDietService.updateDailyDiet(Number(id), dailyDiet);
  res.status(httpStatus.OK).send(updatedDiet);
});

const deleteDailyDiet = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dailyDietService.deleteDailyDiet(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getDailyDiets,
  getDailyDiet,
  createDailyDiet,
  updateDailyDiet,
  deleteDailyDiet
};
