import httpStatus from 'http-status';
import { dietRestrictionService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getDietRestrictions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['avoid']) as { avoid: string };
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const dietRestrictions = await dietRestrictionService.getDietRestrictions(filter, options);
  res.status(httpStatus.OK).send(dietRestrictions);
});

const getDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietRestriction = await dietRestrictionService.getDietRestriction(id);
  res.status(httpStatus.OK).send(dietRestriction);
});

const createDietRestriction = catchAsync(async (req, res) => {
  const dietRestriction = req.body;
  const createdDietRestriction = await dietRestrictionService.createDietRestriction(
    dietRestriction
  );
  res.status(httpStatus.CREATED).send(createdDietRestriction);
});

const updateDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietRestriction = req.body;
  const updatedDietRestriction = await dietRestrictionService.updateDietRestriction(
    Number(id),
    dietRestriction
  );
  res.status(httpStatus.OK).send(updatedDietRestriction);
});

const deleteDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dietRestrictionService.deleteDietRestriction(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getDietRestrictions,
  getDietRestriction,
  createDietRestriction,
  updateDietRestriction,
  deleteDietRestriction
};
