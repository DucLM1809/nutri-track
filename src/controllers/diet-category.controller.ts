import httpStatus from 'http-status';
import { dietCategoryService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getDietCategories = catchAsync(async (req, res) => {
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const dietCategories = await dietCategoryService.getDietCategories(options);
  res.status(httpStatus.OK).send(dietCategories);
});

const getDietCategory = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietCategory = await dietCategoryService.getDietCategory(Number(id));
  res.status(httpStatus.OK).send(dietCategory);
});

const createDietCategory = catchAsync(async (req, res) => {
  const dietCategory = req.body;
  const createdDietCategory = await dietCategoryService.createDietCategory({
    ...dietCategory
  });
  res.status(httpStatus.CREATED).send(createdDietCategory);
});

const updateDietCategory = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietCategory = req.body;
  const updatedDietCategory = await dietCategoryService.updateDietCategory(
    Number(id),
    dietCategory
  );
  res.status(httpStatus.OK).send(updatedDietCategory);
});

const deleteDietCategory = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dietCategoryService.deleteDietCategory(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getDietCategories,
  getDietCategory,
  createDietCategory,
  updateDietCategory,
  deleteDietCategory
};
