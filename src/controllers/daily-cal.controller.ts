import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { dailyCalService } from '../services';

const getDailyCals = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let dailyCals;

  if (role === Role.ADMIN) {
    dailyCals = await dailyCalService.getAllDailyCals(options);
  } else {
    dailyCals = await dailyCalService.getDailyCals(id, options);
  }
  res.status(httpStatus.OK).send(dailyCals);
});

const getDailyCal = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dailyCal = await dailyCalService.getDailyCal(Number(id));
  res.status(httpStatus.OK).send(dailyCal);
});

const createDailyCal = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const dailyCal = req.body;
  const createdDailyCal = await dailyCalService.createDailyCal(id, dailyCal);
  res.status(httpStatus.CREATED).send(createdDailyCal);
});

const updateDailyCal = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dailyCal = req.body;
  const updatedDailyCal = await dailyCalService.updateDailyCal(Number(id), dailyCal);
  res.status(httpStatus.OK).send(updatedDailyCal);
});

const deleteDailyCal = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dailyCalService.deleteDailyCal(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getDailyCals,
  getDailyCal,
  createDailyCal,
  updateDailyCal,
  deleteDailyCal
};
