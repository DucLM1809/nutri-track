import httpStatus from 'http-status';
import { messageService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { ChatEvent } from '../types/chat.interface';
import { User } from '@prisma/client';

const getMessages = catchAsync(async (req, res) => {
  const { chatId } = req.query;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const messages = await messageService.getMessages(
    ChatEvent.GET_MESSAGES,
    { chatId: Number(chatId) },
    options
  );

  res.status(httpStatus.OK).send(messages);
});

const createMessage = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const message = req.body;
  const createdMessage = await messageService.createMessage(ChatEvent.SEND_MESSAGE, {
    ...message,
    senderId: id
  });
  res.status(httpStatus.CREATED).send(createdMessage);
});

const updateMessage = catchAsync(async (req, res) => {
  const { id } = req.params;
  const message = req.body;
  const updatedMessage = await messageService.updateMessage(ChatEvent.SEND_MESSAGE, Number(id), {
    ...message
  });
  res.status(httpStatus.OK).send(updatedMessage);
});

const deleteMessage = catchAsync(async (req, res) => {
  const { id } = req.params;
  await messageService.deleteMessage(ChatEvent.SEND_MESSAGE, Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getMessages,
  createMessage,
  updateMessage,
  deleteMessage
};
