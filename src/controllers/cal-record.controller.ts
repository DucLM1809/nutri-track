import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { calRecordService } from '../services';

const getCalRecords = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let calRecords;

  if (role === Role.ADMIN) {
    calRecords = await calRecordService.getAllCalRecords(options);
  } else {
    calRecords = await calRecordService.getCalRecords(id, options);
  }
  res.status(httpStatus.OK).send(calRecords);
});

const getCalRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  const calRecord = await calRecordService.getCalRecord(Number(id));
  res.status(httpStatus.OK).send(calRecord);
});

const createCalRecord = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const calRecord = req.body;
  const createdCalRecord = await calRecordService.createCalRecord(id, calRecord);
  res.status(httpStatus.CREATED).send(createdCalRecord);
});

const updateCalRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  const calRecord = req.body;
  const updatedCalRecord = await calRecordService.updateCalRecord(Number(id), calRecord);
  res.status(httpStatus.OK).send(updatedCalRecord);
});

const deleteCalRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  await calRecordService.deleteCalRecord(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getCalRecords,
  getCalRecord,
  createCalRecord,
  updateCalRecord,
  deleteCalRecord
};
