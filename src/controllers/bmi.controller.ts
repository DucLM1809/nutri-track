import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import { bmiService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getBmiRecords = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let bmiRecords;

  if (role === Role.ADMIN) {
    bmiRecords = await bmiService.getAllBmiRecords(options);
  } else {
    bmiRecords = await bmiService.getBmiRecords(id, options);
  }
  res.status(httpStatus.OK).send(bmiRecords);
});

const getBmiRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  const bmiRecord = await bmiService.getBmiRecord(Number(id));
  res.status(httpStatus.OK).send(bmiRecord);
});

const createBmiRecord = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const bmiRecord = req.body;
  const createdRecords = await bmiService.createBmiRecord(id, bmiRecord);
  res.status(httpStatus.CREATED).send(createdRecords);
});

const updateBmiRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  const bmiRecord = req.body;
  const updatedRecords = await bmiService.updateBmiRecord(Number(id), bmiRecord);
  res.status(httpStatus.OK).send(updatedRecords);
});

const deleteBmiRecord = catchAsync(async (req, res) => {
  const { id } = req.params;
  await bmiService.deleteBmiRecord(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getBmiRecords,
  getBmiRecord,
  createBmiRecord,
  updateBmiRecord,
  deleteBmiRecord
};
