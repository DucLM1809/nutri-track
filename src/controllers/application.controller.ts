import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import applicationService from '../services/application.service';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const changeApplicationStatus = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  const user = req.user as User;

  const approvedApplication = await applicationService.changeApplicationStatus(
    Number(id),
    status,
    user.id
  );

  res.status(httpStatus.OK).send({
    application: approvedApplication
  });
});

const getApplications = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  let applications;

  if (role === Role.ADMIN) {
    applications = await applicationService.getAllApplications(options);
  } else if (role === Role.EXPERT) {
    applications = await applicationService.getApproverApplications(id, options);
  } else {
    applications = await applicationService.getApplications(id, options);
  }

  res.status(httpStatus.OK).send(applications);
});

const getApplication = catchAsync(async (req, res) => {
  const { id } = req.params;
  const application = await applicationService.getApplication(Number(id));
  res.status(httpStatus.OK).send(application);
});

const createApplication = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const application = req.body;
  const createdApplication = await applicationService.userCreateApplication(id, application);

  res.status(httpStatus.CREATED).send(createdApplication);
});

const updateApplication = catchAsync(async (req, res) => {
  const { id } = req.params;
  const application = req.body;
  const updatedApplication = await applicationService.updateApplication(Number(id), application);
  res.status(httpStatus.OK).send(updatedApplication);
});

const updateApplicationNote = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { note } = req.body;
  const updatedApplication = await applicationService.updateApplicationNote(Number(id), note);
  res.status(httpStatus.OK).send(updatedApplication);
});

const deleteApplication = catchAsync(async (req, res) => {
  const { id } = req.params;
  await applicationService.deleteApplication(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  changeApplicationStatus,
  getApplications,
  createApplication,
  updateApplication,
  deleteApplication,
  getApplication,
  updateApplicationNote
};
