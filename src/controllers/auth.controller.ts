import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import { authService, tokenService } from '../services';
import exclude from '../utils/exclude';
import { Request, Response } from 'express';
import { User } from '@prisma/client';
import emailService from '../services/email.service';

const register = catchAsync(async (req: Request, res: Response) => {
  const { user, bmiRecord, medicalConditionIds, dietRestrictionIds } = req.body;
  const createdUser = await authService.register(
    user,
    bmiRecord,
    medicalConditionIds,
    dietRestrictionIds
  );
  const userWithoutPassword = exclude(createdUser, ['password', 'createdAt', 'updatedAt']);
  const tokens = await tokenService.generateAuthTokens(createdUser);
  res.status(httpStatus.CREATED).send({ user: userWithoutPassword, tokens });
});

const registerExpert = catchAsync(async (req: Request, res: Response) => {
  const { user, application } = req.body;
  const createdUser = await authService.registerExpert(user, application);
  const userWithoutPassword = exclude(createdUser, ['password', 'createdAt', 'updatedAt']);
  const tokens = await tokenService.generateAuthTokens(createdUser);
  res.status(httpStatus.CREATED).send({ user: userWithoutPassword, tokens });
});

const login = catchAsync(async (req, res) => {
  const { email, password, deviceType, deviceToken } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(
    email,
    password,
    deviceToken,
    deviceType
  );
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(req.body.email);
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.body.otp, req.body.email, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

const sendVerificationEmail = catchAsync(async (req, res) => {
  const user = req.user as User;
  const verifyEmailToken = await tokenService.generateVerifyEmailToken(user);
  await emailService.sendVerificationEmail(user.email, verifyEmailToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const verifyEmail = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.query.token as string);
  res.status(httpStatus.NO_CONTENT).send();
});

const loginGoogle = catchAsync(async (req, res) => {
  const userWithoutPassword = exclude(req.user as User, ['password']);
  const tokens = await tokenService.generateAuthTokens(req.user!);
  res.send({ user: userWithoutPassword, tokens });
});

const loginGooglePost = catchAsync(async (req, res) => {
  const { idToken } = req.body;

  const user = await authService.loginGoogle(idToken);
  const tokens = await tokenService.generateAuthTokens(user!);

  res.send({ user: req.user, tokens });
});

const profile = catchAsync(async (req, res) => {
  const { id } = req.user as User;

  const user = await authService.profile(id);

  res.send(user);
});

export default {
  register,
  registerExpert,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  loginGoogle,
  loginGooglePost,
  profile,
  sendVerificationEmail,
  verifyEmail
};
