import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import { notificationService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getNotifications = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let notifications;

  if (role === Role.ADMIN) {
    notifications = await notificationService.getAllNotifications(options);
  } else {
    notifications = await notificationService.getNotifications(id, options);
  }
  res.status(httpStatus.OK).send(notifications);
});

const getNotificationUnreadCount = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const count = await notificationService.getNotificationUnreadCount(id);
  res.status(httpStatus.OK).send({ count });
});

const getNotification = catchAsync(async (req, res) => {
  const { id } = req.params;
  const notification = await notificationService.getNotification(Number(id));
  res.status(httpStatus.OK).send(notification);
});

const createNotification = catchAsync(async (req, res) => {
  const notification = req.body;
  const createdNotification = await notificationService.createNotification(notification);
  res.status(httpStatus.CREATED).send(createdNotification);
});

const updateNotification = catchAsync(async (req, res) => {
  const { id } = req.params;
  const notification = req.body;
  const updatedNotification = await notificationService.updateNotification(
    Number(id),
    notification
  );
  res.status(httpStatus.OK).send(updatedNotification);
});

const readNotification = catchAsync(async (req, res) => {
  const { id } = req.params;
  const notification = await notificationService.readNotification(Number(id));
  res.status(httpStatus.OK).send(notification);
});

const readAllNotifications = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  await notificationService.readAllNotifications(id);
  res.status(httpStatus.NO_CONTENT).send();
});

const unreadCount = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const count = await notificationService.getNotificationUnreadCount(id);
  res.status(httpStatus.OK).send({ count });
});

const deleteNotification = catchAsync(async (req, res) => {
  const { id } = req.params;
  await notificationService.deleteNotification(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

const createNotificationTest = catchAsync(async (req, res) => {
  const notification = req.body;

  await notificationService.pushFcmNotificationTest(notification);
  res.status(httpStatus.CREATED).send();
});

export default {
  getNotifications,
  getNotificationUnreadCount,
  getNotification,
  createNotification,
  updateNotification,
  deleteNotification,
  readNotification,
  readAllNotifications,
  unreadCount,
  createNotificationTest
};
