import httpStatus from 'http-status';
import pick from '../utils/pick';
import ApiError from '../utils/ApiError';
import catchAsync from '../utils/catchAsync';
import { userService } from '../services';

const createUser = catchAsync(async (req, res) => {
  const { email, password, name, role } = req.body;
  // const user = await userService.createUser(email, password, name, role);
  // res.status(httpStatus.CREATED).send(user);
  res.status(httpStatus.CREATED).send();
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);

  res.send(result);
});

const getUsersWithRatings = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const result = await userService.getUsersWithRatings(filter, options);

  res.send(result);
});

const getExperts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const result = await userService.queryExperts(filter, options);

  res.send(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(
    req.params.userId,
    req.body,
    req.body?.medicalConditionIds,
    req.body?.freeTimes
  );
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const updateAccountType = catchAsync(async (req, res) => {
  const user = await userService.updateAccountType(req.params.userId, req.body.accountType);
  res.send(user);
});

export default {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  getUsersWithRatings,
  getExperts,
  updateAccountType
};
