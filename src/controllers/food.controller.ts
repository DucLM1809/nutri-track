import httpStatus from 'http-status';
import { foodService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getFoods = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']) as { name: string };
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const foods = await foodService.getFoods(filter, options);
  res.status(httpStatus.OK).send(foods);
});

const getFood = catchAsync(async (req, res) => {
  const { id } = req.params;
  const food = await foodService.getFood(Number(id));
  res.status(httpStatus.OK).send(food);
});

export default { getFoods, getFood };
