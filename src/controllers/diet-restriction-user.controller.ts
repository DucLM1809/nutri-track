import { User } from '@prisma/client';
import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { dietRestrictionUserService } from '../services';

const getUserDietRestrictions = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const dietRestrictions = await dietRestrictionUserService.getUserDietRestrictions(id, options);
  res.status(httpStatus.OK).send(dietRestrictions);
});

const getUserDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.params;
  const dietRestriction = await dietRestrictionUserService.getUserDietRestriction(id);
  res.status(httpStatus.OK).send(dietRestriction);
});

const createUserDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const { dietRestrictionIds } = req.body;
  const createdDietRestrictions = await dietRestrictionUserService.createUserDietRestriction(
    id,
    dietRestrictionIds
  );
  res.status(httpStatus.CREATED).send(createdDietRestrictions);
});

const deleteUserDietRestriction = catchAsync(async (req, res) => {
  const { id } = req.params;
  await dietRestrictionUserService.deleteUserDietRestriction(id);
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getUserDietRestrictions,
  getUserDietRestriction,
  createUserDietRestriction,
  deleteUserDietRestriction
};
