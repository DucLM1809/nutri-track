import { Role, TransactionStatus, User } from '@prisma/client';
import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { transactionService, walletService } from '../services';
import config from '../config/config';
import moment from 'moment';
import querystring from 'qs';
import crypto from 'crypto';
import prisma from '../client';

const getTransactions = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let transactions;
  if (role === Role.ADMIN) {
    transactions = await transactionService.getTransactions(options);
  } else {
    transactions = await transactionService.getUserTransactions(id, options);
  }
  res.status(httpStatus.OK).send(transactions);
});

const getTransaction = catchAsync(async (req, res) => {
  const { id } = req.params;
  const transaction = await transactionService.getTransaction(Number(id));
  res.status(httpStatus.OK).send(transaction);
});

const createTransaction = catchAsync(async (req, res) => {
  const ipAddr =
    req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress;

  const tmnCode = config.vnpConfig.tmnCode!;
  const secretKey = config.vnpConfig.secretKey!;
  let vnpUrl = config.vnpConfig.url!;
  const returnUrl = config.vnpConfig.returnUrl!;

  const createDate = moment().format('YYYYMMDDHHmmss');

  const { id } = req.user as User;

  const lastTransaction = await prisma.transactions.findFirst({
    orderBy: { id: 'desc' } // Assuming 'id' is the primary key field
  });

  const userWallet = await walletService.getUserWallet(id);
  const orderId = Number(lastTransaction?.id) + 1;
  const amount = req.body.amount;
  const bankCode = 'NCB';
  const orderInfo = req.body.description;

  const locale = 'vn';
  const currCode = 'VND';
  let vnp_Params: any = {};
  vnp_Params['vnp_Amount'] = amount * 100;
  vnp_Params['vnp_Command'] = 'pay';
  vnp_Params['vnp_CreateDate'] = createDate;
  vnp_Params['vnp_CurrCode'] = currCode;
  vnp_Params['vnp_IpAddr'] = ipAddr;
  vnp_Params['vnp_Locale'] = locale;
  vnp_Params['vnp_OrderInfo'] = orderInfo;
  vnp_Params['vnp_OrderType'] = 'other';
  vnp_Params['vnp_ReturnUrl'] = returnUrl;
  vnp_Params['vnp_Version'] = '2.1.0';
  vnp_Params['vnp_TmnCode'] = tmnCode;
  // vnp_Params['vnp_Merchant'] = ''
  vnp_Params['vnp_TxnRef'] = orderId;
  // if (bankCode !== null && bankCode !== '') {
  vnp_Params['vnp_BankCode'] = bankCode;
  // }

  vnp_Params = sortObject(vnp_Params);

  const signData = querystring.stringify(vnp_Params, { encode: false });
  const hmac = crypto.createHmac('sha512', secretKey);
  const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');
  vnp_Params['vnp_SecureHash'] = signed;
  vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: false });

  const transaction = await transactionService.createTransaction(id, {
    amount: amount,
    description: orderInfo,
    walletId: userWallet.id
  });

  res.status(httpStatus.OK).send({
    ...transaction,
    url: vnpUrl
  });
});

const getVnpayIpn = catchAsync(async (req, res) => {
  let vnp_Params = req.query;
  const secureHash = vnp_Params['vnp_SecureHash'];

  delete vnp_Params['vnp_SecureHash'];
  delete vnp_Params['vnp_SecureHashType'];

  vnp_Params = sortObject(vnp_Params);

  const signData = querystring.stringify(vnp_Params, { encode: false });
  const secretKey = config.vnpConfig.secretKey!;
  const hmac = crypto.createHmac('sha512', secretKey);
  const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');

  if (vnp_Params['vnp_TxnRef']) {
    const transaction = await transactionService.updateTransactionStatus(
      Number(vnp_Params['vnp_TxnRef']),
      TransactionStatus.PROCESSING
    );
  }

  if (secureHash === signed) {
    const orderId = vnp_Params['vnp_TxnRef'];
    const rspCode = vnp_Params['vnp_ResponseCode'];
    //Kiem tra du lieu co hop le khong, cap nhat trang thai don hang va gui ket qua cho VNPAY theo dinh dang duoi
    res.status(200).json({ RspCode: '00', Message: 'success' });
  } else {
    res.status(200).json({ RspCode: '97', Message: 'Fail checksum' });
  }
});

const getVnpayReturn = catchAsync(async (req, res) => {
  const vnp_Params = req.query;
  const secureHash = vnp_Params['vnp_SecureHash'];

  delete vnp_Params['vnp_SecureHash'];
  delete vnp_Params['vnp_SecureHashType'];

  const signData = querystring.stringify(vnp_Params, { encode: false });
  const secretKey = config.vnpConfig.secretKey!;
  const hmac = crypto.createHmac('sha512', secretKey);
  const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');

  let transaction;

  if (secureHash === signed) {
    const orderId = vnp_Params['vnp_TxnRef'];
    const rspCode = vnp_Params['vnp_ResponseCode'];
    //Kiem tra du lieu co hop le khong, cap nhat trang thai don hang va gui ket qua cho VNPAY theo dinh dang duoi
    if (vnp_Params['vnp_TxnRef']) {
      transaction = await transactionService.updateTransactionStatus(
        Number(vnp_Params['vnp_TxnRef']),
        TransactionStatus.COMPLETED
      );

      await walletService.updateWallet(transaction.userId, Number(transaction.amount));
    }
  } else {
    if (vnp_Params['vnp_TxnRef']) {
      transaction = await transactionService.updateTransactionStatus(
        Number(vnp_Params['vnp_TxnRef']),
        TransactionStatus.FAILED
      );
    }
  }

  res.redirect(config.vnpConfig.deepLink!);
});

function sortObject(obj: { [key: string]: any }): { [key: string]: string } {
  const sorted: { [key: string]: string } = {};
  const str: string[] = [];
  let key: string;

  for (key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      str.push(encodeURIComponent(key));
    }
  }

  str.sort();

  for (let key = 0; key < str.length; key++) {
    sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, '+');
  }

  return sorted;
}

export default {
  getTransactions,
  getTransaction,
  createTransaction,
  getVnpayIpn,
  getVnpayReturn
};
