import { Role, User } from '@prisma/client';
import httpStatus from 'http-status';
import { dietService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getDiets = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  let diets;

  if (role === Role.ADMIN) {
    diets = await dietService.getAllDiets(options);
  } else {
    diets = await dietService.getDiets(id, options);
  }

  res.status(httpStatus.OK).send(diets);
});

const getDiet = catchAsync(async (req, res) => {
  const { id } = req.params;
  const diet = await dietService.getDiet(Number(id));
  res.status(httpStatus.OK).send(diet);
});

const createDiet = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const diet = req.body;

  let createdDiet;

  if (role === Role.ADMIN) {
    createdDiet = await dietService.createDiet({ ...diet, expertId: id });
  } else {
    createdDiet = await dietService.expertCreateDiet({
      ...diet,
      expertId: id
    });
  }

  res.status(httpStatus.CREATED).send(createdDiet);
});

const updateDiet = catchAsync(async (req, res) => {
  const { role } = req.user as User;
  const { id } = req.params;
  const diet = req.body;

  let updatedDiet;

  if (role === Role.ADMIN) {
    updatedDiet = await dietService.updateDiet(Number(id), diet);
  } else {
    updatedDiet = await dietService.updateDietByApplication(Number(id), diet);
  }

  res.status(httpStatus.OK).send(updatedDiet);
});

const userUpdateDiet = catchAsync(async (req, res) => {
  const { id } = req.params;
  const diet = req.body;

  const updatedDiet = await dietService.userUpdateDiet(Number(id), diet);

  res.status(httpStatus.OK).send(updatedDiet);
});

const deleteDiet = catchAsync(async (req, res) => {
  const { id } = req.params;

  await dietService.deleteDiet(Number(id));

  res.status(httpStatus.NO_CONTENT).send();
});

const changeDietStatus = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { isActive } = req.body;
  const user = req.user as User;

  const approvedApplication = await dietService.changeDietStatus(user.id, Number(id), isActive);

  res.status(httpStatus.OK).send({
    application: approvedApplication
  });
});

export default {
  getDiets,
  getDiet,
  createDiet,
  updateDiet,
  changeDietStatus,
  deleteDiet,
  userUpdateDiet
};
