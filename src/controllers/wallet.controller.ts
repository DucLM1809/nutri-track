import { User } from '@prisma/client';
import httpStatus from 'http-status';
import walletService from '../services/wallet.service';
import catchAsync from '../utils/catchAsync';

const getUserWallet = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const wallet = await walletService.getUserWallet(id);
  res.status(httpStatus.OK).send(wallet);
});

const updateWallet = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const { amount } = req.body;
  await walletService.updateWallet(id, amount);
  res.status(httpStatus.OK).send();
});

export default {
  getUserWallet,
  updateWallet
};
