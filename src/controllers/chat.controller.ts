import httpStatus from 'http-status';
import { chatService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { Role, User } from '@prisma/client';
import { ChatEvent } from '../types/chat.interface';

const getChats = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const { receiverId } = req.query;
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const chats = await chatService.getChats(
    ChatEvent.CHAT_HISTORY,
    {
      userId: role === Role.USER ? id : Number(receiverId),
      expertId: role === Role.EXPERT ? id : Number(receiverId)
    },
    options
  );

  res.status(httpStatus.OK).send(chats);
});

const createChat = catchAsync(async (req, res) => {
  const { id, role } = req.user as User;
  const chat = req.body;
  const createdChat = await chatService.createChat(ChatEvent.JOIN_ROOM, {
    ...chat,
    userId: role === Role.USER ? id : chat.receiverId,
    expertId: role === Role.EXPERT ? id : chat.receiverId
  });
  res.status(httpStatus.CREATED).send(createdChat);
});

const updateChat = catchAsync(async (req, res) => {
  const { id } = req.params;
  const chat = req.body;
  const updatedChat = await chatService.updateChat(Number(id), { ...chat });
  res.status(httpStatus.OK).send(updatedChat);
});

export default {
  getChats,
  createChat,
  updateChat
};
