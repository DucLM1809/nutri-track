import httpStatus from 'http-status';
import { medicalConditionService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getMedicalConditions = catchAsync(async (req, res) => {
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);
  const filter = pick(req.query, ['name']) as { name: string };
  const medicalConditions = await medicalConditionService.getMedicalConditions(filter, options);
  res.status(httpStatus.OK).send(medicalConditions);
});

const getMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.params;
  const medicalCondition = await medicalConditionService.getMedicalCondition(Number(id));
  res.status(httpStatus.OK).send(medicalCondition);
});

const createMedicalCondition = catchAsync(async (req, res) => {
  const medicalCondition = req.body;
  const createdMedicalCondition = await medicalConditionService.createMedicalCondition(
    medicalCondition
  );
  res.status(httpStatus.CREATED).send(createdMedicalCondition);
});

const updateMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.params;
  const medicalCondition = req.body;
  const updatedMedicalCondition = await medicalConditionService.updateMedicalCondition(
    Number(id),
    medicalCondition
  );
  res.status(httpStatus.OK).send(updatedMedicalCondition);
});

const deleteMedicalCondition = catchAsync(async (req, res) => {
  const { id } = req.params;
  await medicalConditionService.deleteMedicalCondition(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getMedicalConditions,
  getMedicalCondition,
  createMedicalCondition,
  updateMedicalCondition,
  deleteMedicalCondition
};
