import { fileService } from '../services';
import catchAsync from '../utils/catchAsync';

const uploadFile = catchAsync(async (req, res) => {
  const { file } = req;
  const fileUploaded = await fileService.uploadFile(file);
  res.status(200).json({ fileUrl: fileUploaded });
});

export default { uploadFile };
