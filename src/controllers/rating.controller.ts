import { User } from '@prisma/client';
import httpStatus from 'http-status';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';
import { ratingService } from '../services';

const getRatings = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['expertId']) as { expertId: number };
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const ratings = await ratingService.getRatings(filter, options);

  res.status(httpStatus.OK).send(ratings);
});

const getRating = catchAsync(async (req, res) => {
  const { id } = req.params;
  const rating = await ratingService.getRating(Number(id));
  res.status(httpStatus.OK).send(rating);
});

const createRating = catchAsync(async (req, res) => {
  const { id } = req.user as User;
  const rating = req.body;
  const createdRating = await ratingService.createRating(id, rating);
  res.status(httpStatus.CREATED).send(createdRating);
});

const updateRating = catchAsync(async (req, res) => {
  const { id } = req.params;
  const rating = req.body;
  const updatedRating = await ratingService.updateRating(Number(id), rating);
  res.status(httpStatus.OK).send(updatedRating);
});

const deleteRating = catchAsync(async (req, res) => {
  const { id } = req.params;
  await ratingService.deleteRating(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getRatings,
  getRating,
  createRating,
  updateRating,
  deleteRating
};
