import httpStatus from 'http-status';
import { mealService } from '../services';
import catchAsync from '../utils/catchAsync';
import pick from '../utils/pick';

const getMeals = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['dietId']);
  const options = pick(req.query, ['sortBy', 'sortType', 'limit', 'page']);

  const meals = await mealService.getMeals(filter, options);

  res.status(httpStatus.OK).send(meals);
});

const getMeal = catchAsync(async (req, res) => {
  const { id } = req.params;
  const meal = await mealService.getMeal(Number(id));
  res.status(httpStatus.OK).send(meal);
});

const createMeal = catchAsync(async (req, res) => {
  const meal = req.body;
  const createdMeal = await mealService.createMeal(meal);
  res.status(httpStatus.CREATED).send(createdMeal);
});

const updateMeal = catchAsync(async (req, res) => {
  const { id } = req.params;
  const meal = req.body;
  const updatedMeal = await mealService.updateMeal(Number(id), meal);
  res.status(httpStatus.OK).send(updatedMeal);
});

const deleteMeal = catchAsync(async (req, res) => {
  const { id } = req.params;
  await mealService.deleteMeal(Number(id));
  res.status(httpStatus.NO_CONTENT).send();
});

export default {
  getMeals,
  getMeal,
  createMeal,
  updateMeal,
  deleteMeal
};
