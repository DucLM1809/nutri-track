import { Socket } from 'socket.io';
import SocketInterface from './socket.interface';

class ChatSocket implements SocketInterface {
  handleConnection(socket: Socket): void {
    socket.emit('Chat connection ready!');
  }

  middlewareImplementation?(socket: Socket, next: any): void {
    next();
  }
}

export default ChatSocket;
