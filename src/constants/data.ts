const DATA_RESTRICTIONS_FILE_PATH = "./data/dietary_restrictions.json"
const DATA_NUTRIENTS_FILE_PATH = "./data/nutrients.json"
const DEFAULT_FOOD_SOURCE = "fdc (FNDDS)";

export default {
    DATA_NUTRIENTS_FILE_PATH,
    DATA_RESTRICTIONS_FILE_PATH,
    DEFAULT_FOOD_SOURCE
}