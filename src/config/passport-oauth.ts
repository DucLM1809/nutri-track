import passport from 'passport';
import passportGoogle from 'passport-google-oauth20';
import config from './config';
import prisma from '../client';
import { userService } from '../services';
const GoogleStrategy = passportGoogle.Strategy;

passport.use(
  new GoogleStrategy(
    {
      clientID: config.googleOAuth.googleClientId,
      clientSecret: config.googleOAuth.googleClientSecret,
      callbackURL: '/v1/auth/google/redirect',
      scope: ['email', 'profile']
    },
    async (accessToken, refreshToken, profile, done) => {
      const existingUser = await userService.getUserByEmail(profile.emails![0].value);

      if (existingUser) {
        done(null, existingUser);
      } else {
        const newUser = await prisma.user.create({
          data: {
            name: profile!.name?.familyName || profile!.username || profile!.displayName,
            email: profile.emails![0].value,
            avatar: profile.photos![0].value
          }
        });

        if (newUser) {
          done(null, newUser);
        }
      }
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await prisma.user.findUnique({ where: { id: Number(id) } });
  done(null, user);
});
