// this service used for import data to database
import * as fs from 'fs';
import logger from '../config/logger';
import prisma from '../client';
import { dataConstants } from '../constants';

interface Nutrient {
  name: string;
  amount: number;
  unitName: string;
}

interface Food {
  description: string;
  foodNutrients: Nutrient[];
}

interface MedicalCondition {
  name: string;
  description: string;
}

interface DietaryRestriction {
  high: string[];
  low: string[];
  avoid: string[];
}

interface MedicalConditionRestriction {
  medicalCondition: MedicalCondition;
  dietaryRestrictions: DietaryRestriction;
}

const importNutientsData = async (filePath: string) => {
  const count = await prisma.foods.count();
  if (count > 0) {
    logger.info('Food data imported');
    return;
  }

  logger.info('Start importing food data');
  fs.readFile(filePath, 'utf8', async (err, data) => {
    if (err) {
      logger.error('Error when import food data: ', err);
      return;
    }

    try {
      const jsonObject = JSON.parse(data);
      const foods = jsonObject as Food[];

      for (const food of foods) {
        // create food
        const dbFood = await prisma.foods.create({
          data: {
            name: food.description,
            description: food.description,
            source: dataConstants.DEFAULT_FOOD_SOURCE
          }
        });

        for (const nutrient of food.foodNutrients) {
          let existedNutrient = await prisma.nutrients.findFirst({
            where: {
              name: nutrient.name
            }
          });

          if (!existedNutrient) {
            existedNutrient = await prisma.nutrients.create({
              data: {
                name: nutrient.name,
                unitName: nutrient.unitName
              }
            });
          }

          // create food nutrients
          await prisma.foodNutrients.create({
            data: {
              foodId: dbFood.id,
              nutrientId: existedNutrient.id,
              amount: nutrient.amount
            }
          });
        }
      }
      logger.info('Import foods successfully');
    } catch (err) {
      logger.error('Error when importing foods: ', err);
    }
  });
};

const importMedicalConditionRestrictionsData = async (filePath: string) => {
  const count = await prisma.medicalConditions.count();
  if (count > 0) {
    logger.info('Restrictions data imported');
    return;
  }

  logger.info('Start importing restrictions data');
  fs.readFile(filePath, 'utf8', async (err, data) => {
    if (err) {
      logger.error('Error when import medical conditions data: ', err);
      return;
    }
    try {
      const jsonObject = JSON.parse(data);
      const restrictions = jsonObject as MedicalConditionRestriction[];
      for (const con of restrictions) {
        await prisma.medicalConditions.create({
          data: {
            name: con.medicalCondition.name,
            description: con.medicalCondition.description,
            high: con.dietaryRestrictions.high.join(','),
            low: con.dietaryRestrictions.low.join(','),
            avoid: con.dietaryRestrictions.avoid.join(',')
          }
        });
      }
      logger.info('Restrictions data imported successfully');
    } catch (err) {
      logger.error('Error when importing restrictions data: ', err);
    }
  });
};

const importRestrictionsData = async (filePath: string) => {
  const count = await prisma.dietRestrictions.count();
  if (count > 0) {
    logger.info('Restrictions data imported');
    return;
  }

  logger.info('Start importing restrictions data');
  fs.readFile(filePath, 'utf8', async (err, data) => {
    if (err) {
      logger.error('Error when import medical conditions data: ', err);
      return;
    }
    try {
      const jsonObject = JSON.parse(data);
      const restrictions = jsonObject as MedicalConditionRestriction[];
      for (const con of restrictions) {
        await prisma.dietRestrictions.create({
          data: {
            high: con.dietaryRestrictions.high.join(','),
            low: con.dietaryRestrictions.low.join(','),
            avoid: con.dietaryRestrictions.avoid.join(',')
          }
        });
      }
      logger.info('Restrictions data imported successfully');
    } catch (err) {
      logger.error('Error when importing restrictions data: ', err);
    }
  });
};
export default {
  importNutientsData,
  importMedicalConditionRestrictionsData,
  importRestrictionsData
};
