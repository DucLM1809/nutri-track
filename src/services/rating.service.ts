import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateRating, UpdateRating } from '../types/rating';

const getRatings = async (
  filter: { expertId: number },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.ratings.count({
      ...(filter.expertId && {
        where: {
          expertId: Number(filter.expertId)
        }
      })
    }),
    prisma.ratings.findMany({
      ...(filter.expertId && {
        where: {
          expertId: Number(filter.expertId)
        }
      }),
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getUserRatings = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.ratings.count({ where: { OR: [{ userId, expertId: userId }] } }),
    prisma.ratings.findMany({
      where: { OR: [{ userId, expertId: userId }] },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getRating = async (id: number) => {
  const rating = await prisma.ratings.findUnique({ where: { id } });

  if (!rating) throw new ApiError(httpStatus.NOT_FOUND, 'Rating not found');

  return rating;
};

const createRating = async (userId: number, rating: CreateRating) => {
  try {
    return await prisma.ratings.create({
      data: {
        ...rating,
        userId
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating rating');
  }
};

const updateRating = async (id: number, rating: UpdateRating) => {
  await getRating(id);

  try {
    return await prisma.ratings.update({
      where: { id },
      data: rating
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating rating');
  }
};

const deleteRating = async (id: number) => {
  await getRating(id);

  try {
    return await prisma.ratings.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting rating');
  }
};

export default {
  getRatings,
  getUserRatings,
  getRating,
  createRating,
  updateRating,
  deleteRating
};
