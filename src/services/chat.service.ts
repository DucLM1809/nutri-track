import httpStatus from 'http-status';
import { CreateChat, UpdateChat } from '../types/chat';
import ApiError from '../utils/ApiError';
import prisma from '../client';
import { Chats } from '@prisma/client';
import Websocket from '../socket';
import exclude from '../utils/exclude';
import { ChatEvent } from '../types/chat.interface';

const getChats = async (
  eventName: ChatEvent,
  filter: { userId: number; expertId: number },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.chats.count({ where: filter }),
    prisma.chats.findMany({
      where: filter,
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  const finalResults = {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };

  updateSockets(eventName, finalResults);

  return finalResults;
};

const getChat = async (id: number) => {
  const chat = await prisma.chats.findUnique({ where: { id } });

  if (!chat) throw new ApiError(httpStatus.NOT_FOUND, 'Chat not found');

  return chat;
};

const findUserExistingChat = async (userId: number, expertId: number) => {
  const chat = await prisma.chats.findFirst({ where: { userId, expertId } });

  return chat;
};

const createChat = async (eventName: ChatEvent, chat: CreateChat) => {
  const { userId, expertId } = chat;
  const chatData = exclude(chat, ['receiverId']) as Chats;
  const existingChatRoom = await findUserExistingChat(userId, expertId);

  if (existingChatRoom) {
    updateSockets('join-room', existingChatRoom);
    return existingChatRoom;
  }

  try {
    const chatRoom = await prisma.chats.create({
      data: chatData
    });

    updateSockets(eventName, chatRoom);

    return chatRoom;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const updateChat = async (id: number, chat: UpdateChat) => {
  await getChat(id);
  try {
    return await prisma.chats.update({
      where: {
        id
      },
      data: chat
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const updateSockets = (eventName: string, eventData: any) => {
  const io = Websocket.getInstance();
  io.of('/v1/chats').emit(eventName, { data: eventData });
};

export default {
  createChat,
  getChat,
  updateChat,
  getChats,
  findUserExistingChat
};
