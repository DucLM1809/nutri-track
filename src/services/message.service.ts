import httpStatus from 'http-status';
import ApiError from '../utils/ApiError';
import prisma from '../client';
import { CreateMessage, UpdateMessage } from '../types/message';
import { ChatEvent } from '../types/chat.interface';
import Websocket from '../socket';
import chatService from './chat.service';
import notificationService from './notification.service';

const getMessages = async (
  eventName: ChatEvent,
  filter: { chatId: number },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.messages.count({
      where: {
        ...filter
      }
    }),
    prisma.messages.findMany({
      where: {
        ...filter
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  const finalResults = {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };

  updateSockets(eventName, finalResults);

  return finalResults;
};

const getMessage = async (id: number) => {
  const message = await prisma.messages.findUnique({ where: { id } });

  if (!message) throw new ApiError(httpStatus.NOT_FOUND, 'Message not found');

  return message;
};

const createMessage = async (eventName: ChatEvent, message: CreateMessage) => {
  const chat = await chatService.getChat(message.chatId);

  try {
    const [newMessage] = await prisma.$transaction([
      prisma.messages.create({
        data: message
      }),
      message.senderId !== chat.expertId
        ? prisma.notifications.create({
            data: {
              userId: chat.expertId,
              title: 'New Message',
              content: message.content
            }
          })
        : prisma.notifications.create({
            data: {
              userId: message.senderId,
              title: 'New Message',
              content: message.content
            }
          })
    ]);

    await notificationService.pushFcmNotification(
      message.senderId !== chat.expertId ? chat.expertId : message.senderId,
      {
        title: 'New Message',
        body: message.content
      }
    );

    updateSockets(eventName, newMessage);

    return newMessage;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const updateMessage = async (eventName: ChatEvent, id: number, message: UpdateMessage) => {
  const existingMessage = await getMessage(id);

  const chat = await chatService.getChat(existingMessage.chatId);

  try {
    const [updatedMessage] = await prisma.$transaction([
      prisma.messages.update({
        where: {
          id
        },
        data: message
      }),
      existingMessage.senderId !== chat.expertId
        ? prisma.notifications.create({
            data: {
              userId: chat.expertId,
              title: 'Update Message',
              content: 'Message has been updated'
            }
          })
        : prisma.notifications.create({
            data: {
              userId: existingMessage.senderId,
              title: 'Update Message',
              content: 'Message has been updated'
            }
          })
    ]);

    await notificationService.pushFcmNotification(
      existingMessage.senderId !== chat.expertId ? chat.expertId : existingMessage.senderId,
      {
        title: 'New Message',
        body: message.content
      }
    );

    updateSockets(eventName, updatedMessage);

    return updatedMessage;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const deleteMessage = async (eventName: ChatEvent, id: number) => {
  await getMessage(id);
  try {
    updateSockets(eventName);
    return await prisma.messages.update({
      where: {
        id
      },
      data: {
        isDeleted: true
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const updateSockets = (eventName: string, eventData?: any) => {
  const io = Websocket.getInstance();
  io.of('/v1/messages').emit(eventName, { data: eventData });
};

export default {
  createMessage,
  getMessages,
  getMessage,
  updateMessage,
  deleteMessage
};
