import { AccountType, ApplicationStatus, ApplicationType } from '@prisma/client';
import httpStatus from 'http-status';
import prisma from '../client';
import { CreateApplication } from '../types/application';
import ApiError from '../utils/ApiError';
import userService from './user.service';
import dietService from './diet.service';
import {
  DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS,
  DEFAULT_BASIC_TYPE_MAX_CUSTOMERS
} from '../constants/common';
import notificationService from './notification.service';
import { mapApplicationStatus } from '../utils/mapApplicationStatus';
import moment from 'moment';

const getAllApplications = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.applications.count(),
    prisma.applications.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getApplications = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.applications.count({ where: { userId } }),
    prisma.applications.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getApproverApplications = async (
  approvedById: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.applications.count({ where: { approvedById } }),
    prisma.applications.findMany({
      where: { approvedById },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getApplication = async (id: number) => {
  const application = await prisma.applications.findUnique({ where: { id } });

  if (!application) throw new ApiError(httpStatus.NOT_FOUND, 'Application not found');

  return application;
};

const changeApplicationStatus = async (id: number, status: ApplicationStatus, userId: number) => {
  const user = await userService.getUserById(userId);

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const application = await getApplication(id);

  if (application.status === ApplicationStatus.APPROVED && status === ApplicationStatus.APPROVED) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Application already approved');
  }

  if (application.type === ApplicationType.UPDATE_DIET && status === ApplicationStatus.APPROVED) {
    if (application.sourceId && application.targetWeight) {
      await prisma.diets.update({
        data: {
          targetWeight: application.targetWeight
        },
        where: {
          id: application.sourceId
        }
      });
    }

    if (application.sourceId && application.endDate) {
      const diet = await dietService.getDiet(application.sourceId);

      if (moment(application.endDate).isBefore(diet.endDate)) {
        const dailyDiets = await prisma.dailyDiets.findMany({
          where: {
            date: {
              gte: application.endDate
            }
          }
        });

        await prisma.meals.deleteMany({
          where: {
            dayId: {
              in: dailyDiets.map((dailyDiet) => dailyDiet.id)
            }
          }
        });

        await prisma.dailyDiets.deleteMany({
          where: {
            date: {
              gte: application.endDate
            }
          }
        });
      }

      await prisma.diets.update({
        data: {
          endDate: application.endDate
        },
        where: {
          id: application.sourceId
        }
      });
    }
  }

  const [approvedApplication] = await prisma.$transaction([
    prisma.applications.update({
      where: {
        id
      },
      data: {
        status,
        approvedById: userId
      }
    }),
    prisma.notifications.create({
      data: {
        userId: application.userId,
        title: `Application ${mapApplicationStatus(status)}`,
        content: `Your application to ${user.name} has been ${status.toLowerCase()}`
      }
    })
  ]);

  await notificationService.pushFcmNotification(application.userId, {
    title: `Application ${mapApplicationStatus(status)}`,
    body: `Your application to ${user.name} has been ${status.toLowerCase()}`
  });

  if (
    approvedApplication.type === ApplicationType.REGISTER_EXPERT &&
    status === ApplicationStatus.APPROVED
  ) {
    const profile = {
      certImage: approvedApplication.image,
      description: approvedApplication.description
    };

    await userService.createExpertProfile(approvedApplication.userId, profile);
  }

  return approvedApplication;
};

const userCreateApplication = async (userId: number, application: CreateApplication) => {
  if (application.type === ApplicationType.CREATE_DIET) {
    const customer = await userService.getUserById(userId);

    const customerDietsCount = await dietService.countCustomerDiets(userId);

    if (
      customer?.accountType === AccountType.BASIC &&
      customerDietsCount >= DEFAULT_BASIC_TYPE_MAX_CUSTOMERS
    ) {
      throw new ApiError(
        httpStatus.BAD_REQUEST,
        `This account has reached maximum ${DEFAULT_BASIC_TYPE_MAX_CUSTOMERS} diets`
      );
    } else if (
      customer?.accountType === AccountType.ADVANCE &&
      customerDietsCount >= DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS
    ) {
      throw new ApiError(
        httpStatus.BAD_REQUEST,
        `This account has reached maximum ${DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS} diets`
      );
    } else {
      return await createApplication(userId, application);
    }
  }

  if (application.type === ApplicationType.UPDATE_DIET) {
    const existingUpdateDietApplication = await prisma.applications.findFirst({
      where: {
        sourceId: application.sourceId,
        status: ApplicationStatus.PENDING,
        approvedById: application.approvedById,
        userId
      }
    });

    if (existingUpdateDietApplication) {
      throw new ApiError(
        httpStatus.BAD_REQUEST,
        `You have already created an application to update diet id ${application.sourceId}`
      );
    }
  }

  return await createApplication(userId, application);
};

const createApplication = async (userId: number, application: CreateApplication) => {
  try {
    const [newApplication] = await prisma.$transaction([
      prisma.applications.create({
        data: {
          ...application,
          status: ApplicationStatus.PENDING,
          userId
        }
      }),
      prisma.notifications.create({
        data: {
          userId: application.approvedById,
          title: 'Application Created',
          content: 'A new application has been created and waiting for your approval'
        }
      })
    ]);

    await notificationService.pushFcmNotification(application.approvedById, {
      title: 'Application Created',
      body: 'A new application has been created and waiting for your approval'
    });

    return newApplication;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating application');
  }
};

const updateApplication = async (id: number, application: CreateApplication) => {
  await getApplication(id);

  try {
    return await prisma.applications.update({
      where: { id },
      data: application
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating application');
  }
};

const updateApplicationNote = async (id: number, note: string) => {
  await getApplication(id);

  try {
    return await prisma.applications.update({
      where: { id },
      data: {
        note
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating application note');
  }
};

const deleteApplication = async (id: number) => {
  await getApplication(id);

  try {
    return await prisma.applications.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting application');
  }
};

export default {
  changeApplicationStatus,
  getAllApplications,
  getApplications,
  getApproverApplications,
  getApplication,
  createApplication,
  updateApplication,
  deleteApplication,
  userCreateApplication,
  updateApplicationNote
};
