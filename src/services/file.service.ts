import httpStatus from 'http-status';
import ApiError from '../utils/ApiError';
import { getStorage, ref, getDownloadURL, uploadBytesResumable } from 'firebase/storage';
import giveCurrentDateTime from '../utils/currentDateTime';
import { initializeApp } from 'firebase/app';
import config from '../config/config';

// //Initialize a firebase application
initializeApp(config.firebase);

// Initialize Cloud Storage and get a reference to the service
const storage = getStorage();

const uploadFile = async (file: any) => {
  if (!file) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'No file uploaded');
  }

  try {
    const dateTime = giveCurrentDateTime();

    const storageRef = ref(storage, `files/${file.originalname + '       ' + dateTime}`);

    // Create file metadata including the content type
    const metadata = {
      contentType: file.mimetype
    };

    // Upload the file in the bucket storage
    const snapshot = await uploadBytesResumable(storageRef, file.buffer, metadata);
    //by using uploadBytesResumable we can control the progress of uploading like pause, resume, cancel

    // Grab the public url
    const downloadURL = await getDownloadURL(snapshot.ref);

    return downloadURL;
  } catch (error) {
    console.log(error);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Error uploading file');
  }
};

export default { uploadFile };
