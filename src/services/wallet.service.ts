import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateWallet } from '../types/wallet';
import logger from '../config/logger';

const getUserWallet = async (userId: number) => {
  const existingWallet = await prisma.wallets.findFirst({ where: { userId } });

  if (!existingWallet) {
    return await createWallet(userId, {
      balance: 0,
      previousBalance: 0,
      lastDeposit: new Date().toISOString()
    });
  }

  return existingWallet;
};

const createWallet = async (userId: number, wallet: CreateWallet) => {
  try {
    return await prisma.wallets.create({
      data: {
        ...wallet,
        userId
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating wallet');
  }
};

const updateWallet = async (id: number, amount: number) => {
  const userWallet = await getUserWallet(id);
  logger.info(`User wallet: ${JSON.stringify(userWallet)}`);

  try {
    return await prisma.wallets.update({
      where: { id: userWallet.id },
      data: {
        balance: {
          ...(amount > 0 ? { increment: amount } : { decrement: Math.abs(amount) })
        },
        previousBalance: userWallet.balance,
        lastDeposit: new Date().toISOString()
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating wallet');
  }
};

export default {
  getUserWallet,
  createWallet,
  updateWallet
};
