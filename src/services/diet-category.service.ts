import httpStatus from 'http-status';
import prisma from '../client';
import { CreateDietCategory } from '../types/diet-category';
import ApiError from '../utils/ApiError';

const getDietCategories = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dietCategories.count(),
    prisma.dietCategories.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDietCategory = async (id: number) => {
  const dietCategory = await prisma.dietCategories.findUnique({ where: { id } });

  if (!dietCategory) throw new ApiError(httpStatus.NOT_FOUND, 'Diet category not found');

  return dietCategory;
};

const createDietCategory = async (dietCategory: CreateDietCategory) => {
  try {
    return await prisma.dietCategories.create({
      data: {
        ...dietCategory,
        high: dietCategory.high,
        low: dietCategory.low
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating diet category');
  }
};

const updateDietCategory = async (id: number, dietCategory: CreateDietCategory) => {
  await getDietCategory(id);

  try {
    return await prisma.dietCategories.update({
      where: { id },
      data: {
        ...dietCategory,
        high: dietCategory.high,
        low: dietCategory.low
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating diet category');
  }
};

const deleteDietCategory = async (id: number) => {
  await getDietCategory(id);

  try {
    return await prisma.dietCategories.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting diet category');
  }
};

export default {
  getDietCategories,
  getDietCategory,
  createDietCategory,
  updateDietCategory,
  deleteDietCategory
};
