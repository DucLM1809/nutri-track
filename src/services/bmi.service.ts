import httpStatus from 'http-status';
import prisma from '../client';
import { CreateBmiRecord } from '../types/bmi';
import ApiError from '../utils/ApiError';

const getAllBmiRecords = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.bmiRecords.count(),
    prisma.bmiRecords.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getBmiRecords = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.bmiRecords.count({ where: { userId } }),
    prisma.bmiRecords.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getBmiRecord = async (id: number) => {
  const bmiRecord = await prisma.bmiRecords.findUnique({ where: { id } });

  if (!bmiRecord) throw new ApiError(httpStatus.NOT_FOUND, 'Bmi record not found');

  return bmiRecord;
};

const createBmiRecord = async (userId: number, bmiRecord: CreateBmiRecord) => {
  try {
    return await prisma.bmiRecords.create({
      data: {
        ...bmiRecord,
        user: {
          connect: {
            id: userId
          }
        }
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating bmi record');
  }
};

const updateBmiRecord = async (id: number, bmiRecord: CreateBmiRecord) => {
  await getBmiRecord(id);

  try {
    return await prisma.bmiRecords.update({
      where: { id },
      data: bmiRecord
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating bmi record');
  }
};

const deleteBmiRecord = async (id: number) => {
  await getBmiRecord(id);

  try {
    return await prisma.bmiRecords.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting bmi record');
  }
};

export default {
  getAllBmiRecords,
  getBmiRecords,
  getBmiRecord,
  createBmiRecord,
  updateBmiRecord,
  deleteBmiRecord
};
