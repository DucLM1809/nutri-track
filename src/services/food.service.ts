import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';

const getFoods = async (
  filter: {
    name: string;
  },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.foods.count({
      where: {
        ...filter,
        name: {
          contains: filter.name,
          mode: 'insensitive'
        }
      }
    }),
    prisma.foods.findMany({
      where: {
        ...filter,
        name: {
          contains: filter.name,
          mode: 'insensitive'
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined,
      include: {
        nutrients: {
          select: {
            id: true,
            nutrient: true,
            amount: true
          }
        }
      }
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getFood = async (id: number) => {
  const food = await prisma.foods.findUnique({
    where: { id },
    include: {
      nutrients: {
        select: {
          id: true,
          nutrient: true,
          amount: true
        }
      }
    }
  });

  if (!food) throw new ApiError(httpStatus.NOT_FOUND, 'Food not found');

  return food;
};

const checkFoodExist = async (ids: number[]) => {
  try {
    const count = await prisma.foods.count({
      where: {
        id: {
          notIn: ids
        }
      }
    });

    return count > 0;
  } catch (err) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, err as string);
  }
};

export default {
  getFoods,
  getFood,
  checkFoodExist
};
