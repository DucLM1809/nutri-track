import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateDailyCal } from '../types/daily-cal';

const getAllDailyCals = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dailyCal.count(),
    prisma.dailyCal.findMany({
      skip: (page - 1) * limit,
      take: limit,
      include: {
        calRecords: true
      },
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDailyCals = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dailyCal.count({
      where: { userId }
    }),
    prisma.dailyCal.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      include: {
        calRecords: true
      },
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDailyCal = async (id: number) => {
  const dailyCal = await prisma.dailyCal.findUnique({
    where: { id },
    include: {
      calRecords: true
    }
  });

  if (!dailyCal) throw new ApiError(httpStatus.NOT_FOUND, 'Daily cal not found');

  return dailyCal;
};

const createDailyCal = async (userId: number, dailyCal: CreateDailyCal) => {
  try {
    return await prisma.dailyCal.create({
      data: {
        userId,
        ...dailyCal
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating daily cal');
  }
};

const updateDailyCal = async (id: number, dailyCal: CreateDailyCal) => {
  await getDailyCal(id);

  try {
    return await prisma.dailyCal.update({
      where: { id },
      data: dailyCal
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating daily cal');
  }
};

const deleteDailyCal = async (id: number) => {
  await getDailyCal(id);

  try {
    return await prisma.dailyCal.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting daily cal');
  }
};

export default {
  getAllDailyCals,
  getDailyCals,
  getDailyCal,
  createDailyCal,
  updateDailyCal,
  deleteDailyCal
};
