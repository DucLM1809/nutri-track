import httpStatus from 'http-status';
import prisma from '../client';
import { CreateNotification } from '../types/notification';
import ApiError from '../utils/ApiError';
import firebase from 'firebase-admin';
import firebaseCert from '../config/firebaseCert.json';

const firebaseAdmin = firebase.initializeApp({
  credential: firebase.credential.cert(firebaseCert as any)
});

const getAllNotifications = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.notifications.count(),
    prisma.notifications.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getNotifications = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.notifications.count({ where: { userId } }),
    prisma.notifications.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getNotificationUnreadCount = async (userId: number) => {
  return prisma.notifications.count({ where: { userId, isRead: false } });
};

const getNotification = async (id: number) => {
  const notification = await prisma.notifications.findUnique({ where: { id } });

  if (!notification) throw new ApiError(httpStatus.NOT_FOUND, 'Notification not found');

  return notification;
};

const createNotification = async (notification: CreateNotification) => {
  try {
    return await prisma.notifications.create({
      data: {
        ...notification
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating notification');
  }
};

const updateNotification = async (id: number, notification: CreateNotification) => {
  await getNotification(id);

  try {
    return await prisma.notifications.update({
      where: { id },
      data: notification
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating notification');
  }
};

const readNotification = async (id: number) => {
  await getNotification(id);

  try {
    return await prisma.notifications.update({
      where: { id },
      data: {
        isRead: true,
        readAt: new Date().toISOString()
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating notification');
  }
};

const readAllNotifications = async (userId: number) => {
  const unreadNotifications = await prisma.notifications.findMany({
    where: { userId, isRead: false }
  });

  try {
    return await prisma.notifications.updateMany({
      data: unreadNotifications.map((notification) => ({
        id: notification.id,
        isRead: true,
        readAt: new Date().toISOString()
      }))
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating notification');
  }
};

const unreadCount = async (userId: number) => {
  try {
    return prisma.notifications.count({ where: { userId, isRead: false } });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error count unread notification');
  }
};

const deleteNotification = async (id: number) => {
  await getNotification(id);

  try {
    return await prisma.notifications.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting notification');
  }
};

const pushFcmNotification = async (
  userId: number,
  notification: { title: string; body: string }
) => {
  const userDevice = await prisma.device.findMany({ where: { userId } });

  if (userDevice.length) {
    userDevice.forEach((device) => {
      const message = {
        token: device.deviceToken,
        notification: {
          title: notification.title,
          body: notification.body
        }
      };

      firebaseAdmin
        .messaging()
        .send(message)
        .then((response) => {
          console.log('Successfully sent message:', response);
        })
        .catch((error) => {
          console.log('Error sending message:', error);
        });
    });
  }
};

const pushFcmNotificationTest = async (notification: {
  deviceToken: string;
  title: string;
  body: string;
}) => {
  const message = {
    token: notification.deviceToken,
    notification: {
      title: notification.title,
      body: notification.body
    }
  };

  firebaseAdmin
    .messaging()
    .send(message)
    .then((response) => {
      console.log('Successfully sent message:', response);
    })
    .catch((error) => {
      console.log('Error sending message:', error);
    });
};

export default {
  getAllNotifications,
  getNotifications,
  getNotificationUnreadCount,
  getNotification,
  createNotification,
  updateNotification,
  deleteNotification,
  pushFcmNotification,
  readNotification,
  readAllNotifications,
  unreadCount,
  pushFcmNotificationTest
};
