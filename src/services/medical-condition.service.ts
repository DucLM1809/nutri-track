import httpStatus from 'http-status';
import prisma from '../client';
import { CreateMedicalCondition } from '../types/medical-condition';
import ApiError from '../utils/ApiError';

const getMedicalConditions = async (
  filter: {
    name: string;
  },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.medicalConditions.count({
      where: {
        ...filter,
        name: {
          contains: filter.name,
          mode: 'insensitive'
        }
      }
    }),
    prisma.medicalConditions.findMany({
      where: {
        ...filter,
        name: {
          contains: filter.name,
          mode: 'insensitive'
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      select: {
        id: true,
        name: true,
        description: true,
        createdAt: true,
        updatedAt: true,
        users: true
      },
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getMedicalCondition = async (id: number) => {
  const medicalCondition = await prisma.medicalConditions.findUnique({
    where: { id }
  });

  if (!medicalCondition) throw new ApiError(httpStatus.NOT_FOUND, 'Medical condition not found');

  return medicalCondition;
};

const createMedicalCondition = async (medicalCondition: CreateMedicalCondition) => {
  try {
    return await prisma.medicalConditions.create({
      data: {
        ...medicalCondition
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating medical condition');
  }
};

const updateMedicalCondition = async (id: number, medicalCondition: CreateMedicalCondition) => {
  await getMedicalCondition(id);

  try {
    return await prisma.medicalConditions.update({
      where: { id },
      data: medicalCondition
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating medical condition');
  }
};

const deleteMedicalCondition = async (id: number) => {
  await getMedicalCondition(id);

  try {
    return await prisma.bmiRecords.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting medical condition');
  }
};

export default {
  getMedicalConditions,
  getMedicalCondition,
  createMedicalCondition,
  updateMedicalCondition,
  deleteMedicalCondition
};
