import httpStatus from 'http-status';
import tokenService from './token.service';
import userService from './user.service';
import ApiError from '../utils/ApiError';
import {
  ApplicationStatus,
  ApplicationType,
  DeviceType,
  Role,
  TokenType,
  User
} from '@prisma/client';
import prisma from '../client';
import { encryptPassword, isPasswordMatch } from '../utils/encryption';
import { AuthTokensResponse } from '../types/response';
import { CreateUser } from '../types/user';
import { CreateBmiRecord } from '../types/bmi';
import exclude from '../utils/exclude';
import { ExpertApplication } from '../types/application';
import { auth } from 'google-auth-library';
import config from '../config/config';
import { roundedToHalf } from '../utils/roundedToHalf';
import { hotp } from 'otplib';

const client = auth.fromAPIKey(config.googleOAuth.googleApiKey);

const register = async (
  user: CreateUser,
  bmiRecord: CreateBmiRecord,
  medicalConditionIds: number[],
  dietRestrictionIds: number[]
) => {
  const { email, password } = user;

  if (await userService.getUserByEmail(email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }

  return prisma.user.create({
    data: {
      ...user,
      password: await encryptPassword(password),
      bmiRecords: {
        create: bmiRecord
      },
      medicalConditions: {
        create: medicalConditionIds.map((id) => ({
          medicalCondition: {
            connect: {
              id
            }
          }
        }))
      },
      dietRestrictions: {
        create: dietRestrictionIds.map((id) => ({
          restriction: {
            connect: {
              id
            }
          }
        }))
      },
      freeTimes: {
        create: user?.freeTimes?.map((freeTime) => ({ ...freeTime }))
      }
    },
    include: {
      bmiRecords: true,
      medicalConditions: true,
      dietRestrictions: true,
      freeTimes: true
    }
  });
};

const registerExpert = async (user: CreateUser, application: ExpertApplication) => {
  const { email, password } = user;

  if (await userService.getUserByEmail(email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }

  const userAdmin = await prisma.user.findFirst({
    where: {
      role: Role.ADMIN
    }
  });

  if (!userAdmin) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'No admin found');
  }

  const registerExpertBody = exclude(user, ['freeTimes']);

  return await prisma.user.create({
    data: {
      ...registerExpertBody,
      password: await encryptPassword(password),
      applications: {
        create: [
          {
            ...application,
            status: ApplicationStatus.PENDING,
            type: ApplicationType.REGISTER_EXPERT,
            approvedById: userAdmin.id
          }
        ]
      }
    },
    include: {
      applications: true
    }
  });
};

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<Omit<User, 'password'>>}
 */
const loginUserWithEmailAndPassword = async (
  email: string,
  password: string,
  deviceToken?: string,
  deviceType?: DeviceType
): Promise<Omit<User, 'password'>> => {
  const user = await userService.getUserByEmail(email, [
    'id',
    'email',
    'name',
    'password',
    'role',
    'avatar',
    'dob',
    'gender',
    'accountType',
    'expertProfileId',
    'startFreeTime',
    'height',
    'endFreeTime',
    'guid',
    'isDeleted',
    'createdAt',
    'updatedAt',
    'isEmailVerified'
  ]);
  if (!user || !(await isPasswordMatch(password, user.password as string))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }

  if (deviceToken) {
    const existingDevice = await prisma.device.findFirst({
      where: {
        deviceToken
      }
    });

    if (!existingDevice) {
      await prisma.device.create({
        data: {
          deviceToken,
          deviceType,
          userId: user.id
        }
      });
    }
  }

  return exclude(user, ['password']);
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise<void>}
 */
const logout = async (refreshToken: string): Promise<void> => {
  const refreshTokenData = await prisma.token.findFirst({
    where: {
      value: refreshToken,
      type: TokenType.REFRESH
    }
  });
  if (!refreshTokenData) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await prisma.token.delete({ where: { id: refreshTokenData.id } });
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<AuthTokensResponse>}
 */
const refreshAuth = async (refreshToken: string): Promise<AuthTokensResponse> => {
  try {
    const refreshTokenData = await tokenService.verifyToken(refreshToken, TokenType.REFRESH);
    const { userId } = refreshTokenData;
    await prisma.token.delete({ where: { id: refreshTokenData.id } });
    return tokenService.generateAuthTokens({ id: userId });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise<void>}
 */
const resetPassword = async (otp: string, email: string, newPassword: string): Promise<void> => {
  const isValid = hotp.check(otp, config.otp.secret, 6);
  console.log('isValid', isValid);

  if (!isValid) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Invalid OTP');
  }

  try {
    // const resetPasswordTokenData = await tokenService.verifyToken(
    //   resetPasswordToken,
    //   TokenType.RESET_PASSWORD
    // );

    const user = await userService.getUserByEmail(email);
    if (!user) {
      throw new Error();
    }
    const encryptedPassword = await encryptPassword(newPassword);
    await userService.updateUserById(user.id, { password: encryptedPassword });
    await prisma.token.deleteMany({ where: { userId: user.id, type: TokenType.RESET_PASSWORD } });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password reset failed');
  }
};

const loginGoogle = async (idToken: string) => {
  try {
    const res = await client.verifyIdToken({ idToken });

    if (res) {
      const userGoogleAuth = res.getPayload()!;
      const { email, name, picture } = userGoogleAuth;

      const existingUser = await prisma.user.findFirst({ where: { email, isDeleted: false } });

      if (existingUser) {
        throw new ApiError(httpStatus.BAD_REQUEST, 'Email has already existed');
      }

      const newUser = await prisma.user.create({
        data: {
          email: email!,
          name: name!,
          avatar: picture
        }
      });

      return exclude(newUser, ['password']);
    }
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const profile = async (userId: number) => {
  try {
    const user = await prisma.user.findUnique({
      where: {
        id: userId
      },
      include: {
        expertRatings: true,
        expertProfile: true,
        medicalConditions: true,
        freeTimes: true
      }
    });

    if (!user) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'User not found');
    }

    const userWithoutPassword = exclude(user, ['password']);

    if (user.role === Role.EXPERT) {
      const averageRatingScore = user.expertRatings.length
        ? roundedToHalf(
            user.expertRatings.reduce((acc, rating) => acc + rating.score, 0) /
              user.expertRatings.length
          )
        : 0;

      return { ...userWithoutPassword, averageRatingScore };
    }

    return userWithoutPassword;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

/**
 * Verify email
 * @param {string} verifyEmailToken
 * @returns {Promise<void>}
 */
const verifyEmail = async (verifyEmailToken: string): Promise<void> => {
  try {
    const verifyEmailTokenData = await tokenService.verifyToken(
      verifyEmailToken,
      TokenType.VERIFY_EMAIL
    );
    await prisma.token.deleteMany({
      where: { userId: verifyEmailTokenData.userId, type: TokenType.VERIFY_EMAIL }
    });
    await userService.updateUserById(verifyEmailTokenData.userId, { isEmailVerified: true });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Email verification failed');
  }
};

export default {
  register,
  registerExpert,
  loginUserWithEmailAndPassword,
  isPasswordMatch,
  encryptPassword,
  logout,
  refreshAuth,
  resetPassword,
  loginGoogle,
  profile,
  verifyEmail
};
