import httpStatus from 'http-status';
import prisma from '../client';
import { CreateDiet, UpdateDiet } from '../types/diet';
import ApiError from '../utils/ApiError';
import dietCategoryService from './diet-category.service';
import userService from './user.service';
import { AccountType, ApplicationStatus, ApplicationType, Role } from '@prisma/client';
import {
  DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS,
  DEFAULT_BASIC_TYPE_MAX_CUSTOMERS
} from '../constants/common';
import logger from '../config/logger';
import notificationService from './notification.service';

const getAllDiets = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.diets.count(),
    prisma.diets.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const countActiveDiets = async (userId: number) => {
  try {
    return await prisma.diets.count({
      where: { userId, isActive: true }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error count customer active diets');
  }
};

const countCustomerDiets = async (userId: number) => {
  try {
    return await prisma.diets.count({
      where: { userId }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error count customer diets');
  }
};

const changeDietStatus = async (userId: number, id: number, isActive: boolean) => {
  const diet = await getDiet(id);

  if (diet.isActive === isActive) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Diet is already in this status');
  }

  if (!isActive) {
    const application = await prisma.applications.findFirst({
      where: {
        sourceId: id,
        type: ApplicationType.UPDATE_DIET,
        status: ApplicationStatus.PENDING
      }
    });

    if (application) {
      await prisma.applications.delete({
        where: {
          id: application.id
        }
      });
    }
  }

  const customerActiveDietsCount = await countActiveDiets(userId);

  if (isActive && customerActiveDietsCount >= 1) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      'One customer can only have 1 active diet at a time'
    );
  } else {
    try {
      const updatedDiet = await prisma.diets.update({
        where: { id },
        data: {
          isActive
        }
      });

      const user = await userService.getUserById(userId);

      if (user?.role === Role.USER) {
        await prisma.notifications.create({
          data: {
            userId: diet.expertId,
            title: "Diet's Status Changed",
            content: `${user.name} has changed their diet's status!`
          }
        });

        await notificationService.pushFcmNotification(diet.expertId, {
          title: "Diet's Status Changed",
          body: `${user.name} has changed their diet's status!`
        });
      }

      return updatedDiet;
    } catch (error) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating diet');
    }
  }
};

const getDiets = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.diets.count({
      where: { OR: [{ userId }, { expertId: userId }] }
    }),
    prisma.diets.findMany({
      where: { OR: [{ userId }, { expertId: userId }] },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDiet = async (id: number) => {
  const diet = await prisma.diets.findUnique({ where: { id } });

  if (!diet) throw new ApiError(httpStatus.NOT_FOUND, 'Diet not found');

  return diet;
};

const countExpertDietsCustomers = async (id: number) => {
  try {
    const dietsDistinct = await prisma.diets.groupBy({
      where: {
        expertId: id
      },
      by: ['userId', 'expertId'],
      _count: {
        userId: true,
        expertId: true
      }
    });

    return dietsDistinct.length;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error count diets');
  }
};

const expertCreateDiet = async (diet: CreateDiet) => {
  let currActiveDiet = true;
  const userApplicationsCount = await prisma.applications.count({
    where: {
      userId: diet.userId,
      type: ApplicationType.CREATE_DIET,
      status: ApplicationStatus.APPROVED
    }
  });

  if (!userApplicationsCount) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      'Customer has no approved diet creation application'
    );
  }

  const { userId, expertId, categoryId } = diet;
  await dietCategoryService.getDietCategory(categoryId);

  const expert = await userService.getUserById(expertId);

  const customerActiveDietsCount = await countActiveDiets(userId);

  if (customerActiveDietsCount >= 1) {
    currActiveDiet = false;
  }

  const customerDietsCount = await countCustomerDiets(userId);
  const expertCustomerCount = await countExpertDietsCustomers(expertId);

  if (
    expert?.accountType === AccountType.BASIC &&
    (expertCustomerCount >= DEFAULT_BASIC_TYPE_MAX_CUSTOMERS ||
      customerDietsCount >= DEFAULT_BASIC_TYPE_MAX_CUSTOMERS)
  ) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `With this account type, expert can only have max ${DEFAULT_BASIC_TYPE_MAX_CUSTOMERS} customers or customers can only have max ${DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS} diets`
    );
  } else if (
    expert?.accountType === AccountType.ADVANCE &&
    customerDietsCount >= DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS
  ) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `With this account type, expert can only have max ${DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS} customers or customers can only have max ${DEFAULT_ADVANCE_TYPE_MAX_CUSTOMERS} diets`
    );
  } else {
    const newDiet = await createDiet({ ...diet, isActive: currActiveDiet });

    await prisma.notifications.create({
      data: {
        userId: expertId,
        title: 'New Diet Created',
        content: 'Your requested diet is ready! Time to dig in!'
      }
    });

    await notificationService.pushFcmNotification(userId, {
      title: 'New Diet Created',
      body: 'Your requested diet is ready! Time to dig in!'
    });

    return newDiet;
  }
};

const createDiet = async (diet: CreateDiet) => {
  try {
    return await prisma.diets.create({
      data: {
        ...diet
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, `Error creating diet: ${error}`);
  }
};

const updateDietByApplication = async (id: number, diet: CreateDiet) => {
  const applicationUpdateDiet = await prisma.applications.findFirst({
    where: { sourceId: id, type: ApplicationType.UPDATE_DIET }
  });

  if (!applicationUpdateDiet) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `You haven't create an application to update diet id ${id}`
    );
  }

  if (applicationUpdateDiet.status !== ApplicationStatus.APPROVED) {
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `Application to update diet id ${id} hasn't been approved`
    );
  }

  return await updateDiet(id, diet);
};

const updateDiet = async (id: number, diet: UpdateDiet) => {
  await dietCategoryService.getDietCategory(diet.categoryId);

  await getDiet(id);

  try {
    return await prisma.diets.update({
      where: { id },
      data: diet
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating diet');
  }
};

const userUpdateDiet = async (id: number, diet: UpdateDiet) => {
  await getDiet(id);

  try {
    return await prisma.diets.update({
      where: { id },
      data: diet
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating diet');
  }
};

const deleteDiet = async (id: number) => {
  await getDiet(id);

  try {
    return await prisma.diets.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting diet');
  }
};

const invalidateDiet = async () => {
  logger.info('Cron job is running. Invalidating diets is ready!');
  try {
    return await prisma.diets.updateMany({
      where: {
        endDate: {
          lt: new Date()
        },
        isActive: true
      },
      data: { isActive: false }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error invalidating diet');
  }
};

export default {
  getAllDiets,
  getDiets,
  getDiet,
  createDiet,
  expertCreateDiet,
  updateDiet,
  updateDietByApplication,
  changeDietStatus,
  deleteDiet,
  countCustomerDiets,
  invalidateDiet,
  userUpdateDiet
};
