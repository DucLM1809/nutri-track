import httpStatus from 'http-status';
import prisma from '../client';
import { CreateMeal } from '../types/meal';
import ApiError from '../utils/ApiError';
import exclude from '../utils/exclude';
import foodService from './food.service';

const getMeals = async (
  filter: {
    dietId?: number;
  },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page;
  const limit = options.limit;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const { dietId } = filter;

  const [totalResults, results] = await prisma.$transaction([
    prisma.meals.count({
      where: {
        daylyDiet: {
          dietId: dietId ? Number(dietId) : undefined
        }
      }
    }),
    prisma.meals.findMany({
      where: {
        daylyDiet: {
          dietId: dietId ? Number(dietId) : undefined
        }
      },
      skip: page && limit ? (page - 1) * limit : undefined,
      take: limit,
      include: {
        daylyDiet: {
          include: {
            diet: true
          }
        },
        mealFoods: {
          include: {
            food: true
          }
        }
      },
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    ...(limit && { totalPages: Math.ceil(totalResults / limit) }),
    ...(page && { page }),
    ...(limit && { limit }),
    totalResults
  };
};

const getMeal = async (id: number) => {
  const meal = await prisma.meals.findUnique({
    where: { id },
    include: {
      mealFoods: {
        include: {
          food: true
        }
      },
      daylyDiet: {
        include: {
          diet: true
        }
      }
    }
  });

  if (!meal) throw new ApiError(httpStatus.NOT_FOUND, 'Meal not found');

  return meal;
};

const createMeal = async (meal: CreateMeal) => {
  const foodIds = meal?.foods?.map((food) => food.id);
  const isFoodIdsValid = await foodService.checkFoodExist(foodIds);

  if (!isFoodIdsValid) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Some food not found');
  }

  const foods = meal.foods;

  try {
    const mealBody = exclude(meal, ['foods']);

    return await prisma.meals.create({
      data: {
        ...mealBody,
        mealFoods: {
          createMany: {
            data: foods?.map((food) => ({
              foodId: food.id,
              amount: food.amount,
              unit: food.unit
            }))
          }
        }
      },
      include: {
        mealFoods: {
          include: {
            food: true
          }
        },
        daylyDiet: {
          include: {
            diet: true
          }
        }
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error as string);
  }
};

const updateMeal = async (id: number, meal: CreateMeal) => {
  await getMeal(id);

  const foods = meal.foods;

  try {
    const mealBody = exclude(meal, ['foods']);

    const [updatedmeal] = await prisma.$transaction([
      prisma.mealFoods.deleteMany({ where: { mealId: id } }),
      prisma.meals.update({
        where: { id },
        data: {
          ...mealBody,
          mealFoods: {
            createMany: {
              data: foods?.map((food) => ({
                foodId: food.id,
                amount: food.amount,
                unit: food.unit
              }))
            }
          }
        },
        include: {
          mealFoods: {
            include: {
              food: true
            }
          },
          daylyDiet: {
            include: {
              diet: true
            }
          }
        }
      })
    ]);

    return updatedmeal;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating meal');
  }
};

const deleteMeal = async (id: number) => {
  await getMeal(id);

  try {
    await prisma.mealFoods.deleteMany({ where: { mealId: id } });
    return await prisma.meals.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting meal');
  }
};

export default {
  getMeals,
  getMeal,
  createMeal,
  updateMeal,
  deleteMeal
};
