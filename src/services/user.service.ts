import { User, Prisma, Role, AccountType } from '@prisma/client';
import httpStatus from 'http-status';
import prisma from '../client';
import { CreateUser, ExpertProfile } from '../types/user';
import ApiError from '../utils/ApiError';
import { encryptPassword } from '../utils/encryption';
import { roundedToHalf } from '../utils/roundedToHalf';
import exclude from '../utils/exclude';

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (user: CreateUser): Promise<User> => {
  const { email, password } = user;

  if (await getUserByEmail(email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }

  return prisma.user.create({
    data: {
      ...user,
      password: await encryptPassword(password),
      freeTimes: {
        create: user?.freeTimes?.map((freeTime) => ({ ...freeTime }))
      }
    }
  });
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async <Key extends keyof User>(
  filter: object,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  },
  keys: Key[] = [
    'id',
    'email',
    'name',
    'role',
    'avatar',
    'dob',
    'height',
    'gender',
    'accountType',
    'expertProfileId',
    'guid',
    'createdAt',
    'updatedAt'
  ] as Key[]
): Promise<Pick<User, Key>[]> => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';
  const users = await prisma.user.findMany({
    where: {
      ...filter,
      isDeleted: false
    },
    select: keys.reduce((obj, k) => ({ ...obj, [k]: true }), {}),
    skip: (page - 1) * limit,
    take: limit,
    orderBy: sortBy ? { [sortBy]: sortType } : undefined
  });
  return users as Pick<User, Key>[];
};

const getUsersWithRatings = async <Key extends keyof User>(
  filter: object,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  },
  keys: Key[] = [
    'id',
    'email',
    'name',
    'role',
    'dob',
    'avatar',
    'gender',
    'height',
    'accountType',
    'guid',
    'createdAt',
    'updatedAt'
  ] as Key[]
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.user.count({
      where: {
        ...filter,
        isDeleted: false
      }
    }),
    prisma.user.findMany({
      where: {
        ...filter,
        isDeleted: false
      },
      select: {
        expertRatings: true,
        ...keys.reduce((obj, k) => ({ ...obj, [k]: true }), {})
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  const userResutls = results.map((user) => ({
    ...user,
    averageScore: user.expertRatings.length
      ? roundedToHalf(
          user.expertRatings.reduce((acc, rating) => acc + rating.score, 0) /
            user.expertRatings.length
        )
      : 0
  }));

  return {
    results: userResutls,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const queryExperts = async <Key extends keyof User>(
  filter: object,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  },
  keys: Key[] = [
    'id',
    'email',
    'name',
    'role',
    'avatar',
    'dob',
    'height',
    'gender',
    'accountType',
    'guid',
    'createdAt',
    'updatedAt'
  ] as Key[]
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';
  const [totalResults, results] = await prisma.$transaction([
    prisma.user.count({
      where: {
        ...filter,
        role: Role.EXPERT,
        isDeleted: false
      }
    }),
    prisma.user.findMany({
      where: {
        ...filter,
        role: Role.EXPERT,
        isDeleted: false
      },
      select: {
        expertRatings: true,
        expertProfile: true,
        ...keys.reduce((obj, k) => ({ ...obj, [k]: true }), {})
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @param {Array<Key>} keys
 * @returns {Promise<Pick<User, Key> | null>}
 */
const getUserById = async <Key extends keyof User>(
  id: number,
  keys: Key[] = [
    'id',
    'email',
    'name',
    'role',
    'avatar',
    'dob',
    'height',
    'gender',
    'accountType',
    'expertProfileId',
    'guid',
    'createdAt',
    'updatedAt'
  ] as Key[]
): Promise<Pick<User, Key> | null> => {
  return prisma.user.findUnique({
    where: { id, isDeleted: false },
    select: {
      freeTimes: true,
      expertProfile: true,
      ...keys.reduce((obj, k) => ({ ...obj, [k]: true }), {})
    }
  }) as Promise<Pick<User, Key> | null>;
};

/**
 * Get user by email
 * @param {string} email
 * @param {Array<Key>} keys
 * @returns {Promise<Pick<User, Key> | null>}
 */
const getUserByEmail = async <Key extends keyof User>(
  email: string,
  keys: Key[] = [
    'id',
    'email',
    'name',
    'role',
    'avatar',
    'dob',
    'height',
    'gender',
    'accountType',
    'expertProfileId',
    'startFreeTime',
    'endFreeTime',
    'guid',
    'createdAt',
    'updatedAt'
  ] as Key[]
): Promise<Pick<User, Key> | null> => {
  return prisma.user.findFirst({
    where: { email, isDeleted: false },
    select: {
      freeTimes: true,
      expertProfile: true,
      ...keys.reduce((obj, k) => ({ ...obj, [k]: true }), {})
    }
  }) as Promise<Pick<User, Key> | null>;
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async <Key extends keyof User>(
  userId: number,
  updateBody: Prisma.UserUpdateInput & { medicalConditionIds?: number[] },
  medicalConditionIds: number[] = [],
  freeTimes: {
    startFreeTime: string;
    endFreeTime: string;
  }[] = [],
  keys: Key[] = ['id', 'email', 'name', 'role'] as Key[]
): Promise<Pick<User, Key> | null> => {
  const user = await getUserById(userId, ['id', 'email', 'name']);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  if (updateBody.email && (await getUserByEmail(updateBody.email as string))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }

  const [updatedUser] = await prisma.$transaction([
    prisma.user.update({
      where: { id: user.id },
      data: exclude(updateBody, ['medicalConditionIds', 'freeTimes']),
      select: keys.reduce((obj, k) => ({ ...obj, [k]: true }), {})
    }),
    prisma.medicalConditionsUser.deleteMany({
      where: {
        userId
      }
    }),
    prisma.medicalConditionsUser.createMany({
      data: medicalConditionIds.map((id) => ({ userId, conditionId: id }))
    }),
    prisma.freeTimes.deleteMany({
      where: {
        userId
      }
    }),
    prisma.freeTimes.createMany({
      data: freeTimes.map((freeTime) => ({
        userId,
        startFreeTime: freeTime.startFreeTime,
        endFreeTime: freeTime.endFreeTime
      }))
    })
  ]);

  return updatedUser as Pick<User, Key> | null;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId: number): Promise<User> => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await prisma.user.update({ where: { id: user.id }, data: { isDeleted: true } });
  return user;
};

const createExpertProfile = async (userId: number, profile: ExpertProfile) => {
  try {
    const user = await getUserById(userId);

    if (!user) {
      throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
    }

    return prisma.user.update({
      where: { id: user.id },
      data: {
        expertProfile: {
          upsert: {
            update: profile,
            create: profile
          }
        },
        role: Role.EXPERT
      },
      include: {
        expertProfile: true
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating expert profile');
  }
};

const updateAccountType = async (userId: number, accountType: AccountType) => {
  const user = await getUserById(userId);

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  return prisma.user.update({
    where: { id: user.id },
    data: {
      accountType
    }
  });
};

export default {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  createExpertProfile,
  getUsersWithRatings,
  queryExperts,
  updateAccountType
};
