import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateDietRestriction } from '../types/diet-restriction';

const getDietRestrictions = async (
  filter: {
    avoid: string;
  },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dietRestrictions.count({
      where: {
        ...filter,
        avoid: {
          contains: filter.avoid,
          mode: 'insensitive'
        }
      }
    }),
    prisma.dietRestrictions.findMany({
      where: {
        ...filter,
        avoid: {
          contains: filter.avoid,
          mode: 'insensitive'
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDietRestriction = async (id: number) => {
  const dietRestriction = await prisma.dietRestrictions.findUnique({
    where: { id }
  });

  if (!dietRestriction) throw new ApiError(httpStatus.NOT_FOUND, 'Diet restriction not found');

  return dietRestriction;
};

const createDietRestriction = async (dietRestriction: CreateDietRestriction) => {
  try {
    return await prisma.dietRestrictions.create({
      data: {
        ...dietRestriction
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating diet restriction');
  }
};

const updateDietRestriction = async (id: number, dietRestriction: CreateDietRestriction) => {
  await getDietRestriction(id);

  try {
    return await prisma.medicalConditions.update({
      where: { id },
      data: dietRestriction
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating diet restriction');
  }
};

const deleteDietRestriction = async (id: number) => {
  await getDietRestriction(id);

  try {
    return await prisma.dietRestrictions.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting diet restriction');
  }
};

export default {
  getDietRestrictions,
  getDietRestriction,
  createDietRestriction,
  updateDietRestriction,
  deleteDietRestriction
};
