import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateTransaction } from '../types/transaction';
import { TransactionStatus } from '@prisma/client';

const getTransactions = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.transactions.count(),
    prisma.transactions.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getUserTransactions = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.transactions.count({ where: { userId } }),
    prisma.transactions.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getTransaction = async (id: number) => {
  const transaction = await prisma.transactions.findUnique({ where: { id } });

  if (!transaction) throw new ApiError(httpStatus.NOT_FOUND, 'Transaction not found');

  return transaction;
};

const createTransaction = async (userId: number, transaction: CreateTransaction) => {
  try {
    return await prisma.transactions.create({
      data: {
        ...transaction,
        userId
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating transaction');
  }
};

const updateTransactionStatus = async (id: number, status: TransactionStatus) => {
  const transaction = await prisma.transactions.update({
    where: { id },
    data: { status }
  });

  return transaction;
};

export default {
  getTransactions,
  getUserTransactions,
  getTransaction,
  createTransaction,
  updateTransactionStatus
};
