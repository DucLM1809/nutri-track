import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateCalRecord, UpdateCalRecord } from '../types/cal-record';

const getAllCalRecords = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  // TODO: get by day cal id
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.calRecords.count(),
    prisma.calRecords.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getCalRecords = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  // TODO: get by day cal id
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.calRecords.count({
      where: {
        dailyCal: {
          userId
        }
      }
    }),
    prisma.calRecords.findMany({
      where: {
        dailyCal: {
          userId
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getCalRecord = async (id: number) => {
  const calRecord = await prisma.calRecords.findUnique({
    where: { id }
  });

  if (!calRecord) throw new ApiError(httpStatus.NOT_FOUND, 'Cal record not found');

  return calRecord;
};

const createCalRecord = async (userId: number, calRecord: CreateCalRecord) => {
  try {
    return await prisma.calRecords.create({
      data: {
        ...calRecord
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating cal record');
  }
};

const updateCalRecord = async (id: number, calRecord: UpdateCalRecord) => {
  await getCalRecord(id);

  try {
    return await prisma.calRecords.update({
      where: { id },
      data: calRecord
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating cal record');
  }
};

const deleteCalRecord = async (id: number) => {
  await getCalRecord(id);

  try {
    return await prisma.calRecords.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting cal record');
  }
};

export default {
  getAllCalRecords,
  getCalRecords,
  getCalRecord,
  createCalRecord,
  updateCalRecord,
  deleteCalRecord
};
