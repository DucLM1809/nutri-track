import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';

const getAllUserDietRestrictions = async (options: {
  limit?: number;
  page?: number;
  sortBy?: string;
  sortType?: 'asc' | 'desc';
}) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dietRestrictionsUser.count(),
    prisma.dietRestrictionsUser.findMany({
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined,
      include: { restriction: true }
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getUserDietRestrictions = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dietRestrictionsUser.count({
      where: { userId }
    }),
    prisma.dietRestrictionsUser.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined,
      include: { restriction: true }
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getUserDietRestriction = async (id: number) => {
  const dietRestriction = await prisma.dietRestrictionsUser.findUnique({
    where: { id },
    include: { restriction: true }
  });

  if (!dietRestriction) throw new ApiError(httpStatus.NOT_FOUND, 'Diet restriction not found');

  return dietRestriction;
};

const createUserDietRestriction = async (userId: number, dietRestrictionIds: number[]) => {
  try {
    return await prisma.$transaction(
      dietRestrictionIds.map((restrictionId) =>
        prisma.dietRestrictionsUser.create({ data: { userId, restrictionId } })
      )
    );
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating diet restriction');
  }
};

const deleteUserDietRestriction = async (id: number) => {
  await getUserDietRestriction(id);

  try {
    return await prisma.medicalConditionsUser.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting medical condition');
  }
};

export default {
  getAllUserDietRestrictions,
  getUserDietRestrictions,
  getUserDietRestriction,
  createUserDietRestriction,
  deleteUserDietRestriction
};
