import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';
import { CreateDailyDietReport, UpdateDailyDietReport } from '../types/daily-diet-report';
import dailyDietService from './daily-diet.service';
import { ReportStatus } from '@prisma/client';
import notificationService from './notification.service';
import walletService from './wallet.service';
import {
  DEFAULT_INCREMENT_BALANCE,
  DEFAULT_INCREMENT_MAX_BALANCE,
  DEFAULT_REASONABLE_SCORE
} from '../constants/common';
import dietService from './diet.service';

const getAllDailyDietReports = async (
  filter: { dietId: number },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dailyDietReports.count({
      where: {
        dailyDiet: {
          dietId: Number(filter.dietId)
        }
      }
    }),
    prisma.dailyDietReports.findMany({
      where: {
        dailyDiet: {
          dietId: Number(filter.dietId)
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDailyDietReports = async (
  userId: number,
  filter: { dietId: number },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.dailyDietReports.count({
      where: {
        dailyDiet: {
          dietId: filter.dietId,
          diet: {
            OR: [{ userId }, { expertId: userId }]
          }
        }
      }
    }),
    prisma.dailyDietReports.findMany({
      where: {
        dailyDiet: {
          dietId: filter.dietId,
          diet: {
            OR: [{ userId }, { expertId: userId }]
          }
        }
      },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getDailyDietReport = async (id: number) => {
  const dietReport = await prisma.dailyDietReports.findUnique({ where: { id } });

  if (!dietReport) throw new ApiError(httpStatus.NOT_FOUND, 'Daily diet report not found');

  return dietReport;
};

const createDailyDietReport = async (dailyDietReport: CreateDailyDietReport) => {
  await dailyDietService.getDailyDiet(dailyDietReport.dayId);

  try {
    const [newDietReport] = await prisma.$transaction([
      prisma.dailyDietReports.create({
        data: {
          ...dailyDietReport
        }
      }),
      prisma.notifications.create({
        data: {
          userId: dailyDietReport.expertId,
          title: 'New Daily Diet Report',
          content: "You've got a new daily diet report! Claim rewards now!"
        }
      })
    ]);

    await notificationService.pushFcmNotification(dailyDietReport.expertId, {
      title: 'New Daily Diet Report',
      body: "You've got a new daily diet report! Claim rewards now"
    });

    const dailyDiet = await dailyDietService.getDailyDiet(dailyDietReport.dayId);
    const diet = await dietService.getDiet(dailyDiet.dietId);

    if (
      dailyDietReport.status === ReportStatus.COMPLETE &&
      dailyDietReport.score >= DEFAULT_REASONABLE_SCORE
    ) {
      if (newDietReport.createdAt === diet.endDate) {
        await walletService.updateWallet(dailyDietReport.expertId, DEFAULT_INCREMENT_MAX_BALANCE);
      } else {
        await walletService.updateWallet(dailyDietReport.expertId, DEFAULT_INCREMENT_BALANCE);
      }
    }

    return newDietReport;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating daily diet report');
  }
};

const updateDailyDietReport = async (id: number, dietReport: UpdateDailyDietReport) => {
  const dailyDietReport = await getDailyDietReport(id);

  try {
    const updatedDietReport = await prisma.dailyDietReports.update({
      where: { id },
      data: dietReport
    });

    if (
      updatedDietReport.expertId &&
      updatedDietReport.status !== ReportStatus.INCOMPLETE &&
      updatedDietReport.score >= DEFAULT_REASONABLE_SCORE &&
      dailyDietReport.score < DEFAULT_REASONABLE_SCORE
    ) {
      await walletService.updateWallet(updatedDietReport.expertId, DEFAULT_INCREMENT_BALANCE);
    }
    return updatedDietReport;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating daily diet report');
  }
};

const deleteDailyDietReport = async (id: number) => {
  await getDailyDietReport(id);

  try {
    return await prisma.dailyDietReports.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting daily diet report');
  }
};

const dailyDietReportStatistic = async (
  score?: number,
  status?: ReportStatus,
  from?: string,
  to?: string
) => {
  try {
    const currentDate = from ? new Date(from) : new Date();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    const endOfMonth = new Date(year, month, 0);

    const [totalScoreMatch, totalStatusMatch] = await prisma.$transaction([
      prisma.dailyDietReports.count({
        where: {
          score,
          createdAt: {
            gte: from || currentDate.toISOString(),
            lte: to || endOfMonth.toISOString()
          }
        }
      }),
      prisma.dailyDietReports.count({
        where: {
          status,
          createdAt: {
            gte: from || currentDate.toISOString(),
            lte: to || endOfMonth.toISOString()
          }
        }
      })
    ]);

    return { totalScoreMatch, totalStatusMatch };
  } catch (error) {
    throw new ApiError(httpStatus.BAD_GATEWAY, error as string);
  }
};

export default {
  getAllDailyDietReports,
  getDailyDietReport,
  getDailyDietReports,
  createDailyDietReport,
  updateDailyDietReport,
  deleteDailyDietReport,
  dailyDietReportStatistic
};
