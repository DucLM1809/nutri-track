import httpStatus from 'http-status';
import prisma from '../client';
import { CreateDailyDiet, UpdateDailyDiet } from '../types/daily-diet';
import ApiError from '../utils/ApiError';
import dietService from './diet.service';

const getDailyDiets = async (
  filter: {
    dietId?: number;
  },
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page;
  const limit = options.limit;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const { dietId } = filter;

  const [totalResults, results] = await prisma.$transaction([
    prisma.dailyDiets.count({
      where: {
        dietId: dietId ? Number(dietId) : undefined
      }
    }),
    prisma.dailyDiets.findMany({
      where: {
        dietId: dietId ? Number(dietId) : undefined
      },
      skip: page && limit ? (page - 1) * limit : undefined,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined
    })
  ]);

  return {
    results,
    ...(limit && { totalPages: Math.ceil(totalResults / limit) }),
    ...(page && { page }),
    ...(limit && { limit }),
    totalResults
  };
};

const getDailyDiet = async (id: number) => {
  const diet = await prisma.dailyDiets.findUnique({ where: { id } });

  if (!diet) throw new ApiError(httpStatus.NOT_FOUND, 'Daily diet not found');

  return diet;
};

const createDailyDiet = async (dailyDiet: CreateDailyDiet) => {
  await dietService.getDiet(dailyDiet.dietId);

  try {
    return await prisma.dailyDiets.create({
      data: {
        ...dailyDiet
      }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating daily diet');
  }
};

const updateDailyDiet = async (id: number, diet: UpdateDailyDiet) => {
  await getDailyDiet(id);

  try {
    return await prisma.dailyDiets.update({
      where: { id },
      data: diet
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error updating daily diet');
  }
};

const deleteDailyDiet = async (id: number) => {
  await getDailyDiet(id);

  try {
    return await prisma.dailyDiets.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting daily diet');
  }
};

export default {
  getDailyDiets,
  getDailyDiet,
  createDailyDiet,
  updateDailyDiet,
  deleteDailyDiet
};
