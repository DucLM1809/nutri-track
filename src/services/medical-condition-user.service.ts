import httpStatus from 'http-status';
import prisma from '../client';
import ApiError from '../utils/ApiError';

const getUserMedicalConditions = async (
  userId: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.medicalConditionsUser.count({
      where: { userId }
    }),
    prisma.medicalConditionsUser.findMany({
      where: { userId },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined,
      include: { medicalCondition: true }
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

const getUserMedicalCondition = async (id: number) => {
  const medicalCondition = await prisma.medicalConditionsUser.findUnique({
    where: { id },
    include: { medicalCondition: true }
  });

  if (!medicalCondition) throw new ApiError(httpStatus.NOT_FOUND, 'Medical condition not found');

  return medicalCondition;
};

const createUserMedicalCondition = async (userId: number, medicalConditionIds: number[]) => {
  try {
    return await prisma.$transaction(
      medicalConditionIds.map((conditionId) =>
        prisma.medicalConditionsUser.create({ data: { userId, conditionId } })
      )
    );
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error creating medical condition');
  }
};

const deleteUserMedicalCondition = async (id: number) => {
  await getUserMedicalCondition(id);

  try {
    return await prisma.medicalConditionsUser.delete({
      where: { id }
    });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Error deleting medical condition');
  }
};

const getMedicalConditionOfUser = async (
  id: number,
  options: {
    limit?: number;
    page?: number;
    sortBy?: string;
    sortType?: 'asc' | 'desc';
  }
) => {
  const page = options.page ?? 1;
  const limit = options.limit ?? 10;
  const sortBy = options.sortBy;
  const sortType = options.sortType ?? 'asc';

  const [totalResults, results] = await prisma.$transaction([
    prisma.medicalConditionsUser.count({
      where: { userId: id }
    }),
    prisma.medicalConditionsUser.findMany({
      where: { userId: id },
      skip: (page - 1) * limit,
      take: limit,
      orderBy: sortBy ? { [sortBy]: sortType } : undefined,
      include: { medicalCondition: true }
    })
  ]);

  return {
    results,
    totalPages: Math.ceil(totalResults / limit),
    page,
    limit,
    totalResults
  };
};

export default {
  getUserMedicalConditions,
  getUserMedicalCondition,
  createUserMedicalCondition,
  deleteUserMedicalCondition,
  getMedicalConditionOfUser
};
