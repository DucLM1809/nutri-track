export interface CreateBmiRecord {
  weight: number;
  date: Date;
}
