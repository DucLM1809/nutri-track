export interface CreateMedicalCondition {
  name: string;
  description?: string;
  high?: string;
  low?: string;
  avoid?: string;
}
