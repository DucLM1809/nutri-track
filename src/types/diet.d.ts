export interface CreateDiet {
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  initialWeight: number;
  targetWeight: number;
  actualWeight: number;
  isActive: boolean;
  expertId: number;
  userId: number;
  categoryId: number;
}

export interface UpdateDiet {
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  initialWeight: number;
  targetWeight: number;
  actualWeight: number;
  isActive: boolean;
  expertId: number;
  categoryId: number;
}
