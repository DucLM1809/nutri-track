export interface CreateMessage {
  content: string;
  chatId: number;
  senderId: number;
}

export interface UpdateMessage {
  content: string;
}
