export interface CreateNotification {
  title: string;
  content: string;
  userId: number;
}
