import { ChatStatus, ChatType } from '@prisma/client';

export interface CreateChat {
  userId: number;
  expertId: number;
  type?: ChatType;
  status?: ChatStatus;
  receiverId: number;
}

export interface UpdateChat {
  status?: ChatStatus;
  type?: ChatType;
}
