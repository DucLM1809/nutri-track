export interface CreateDietRestriction {
  high: string;
  low: string;
  avoid: string;
}
