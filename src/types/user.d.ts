export interface CreateUser {
  email: string;
  password: string;
  name: string;
  avatar: string;
  height: number;
  dob: Date;
  description?: string;
  startFreeTime?: Date;
  endFreeTime: Date;
  freeTimes?: {
    startFreeTime: Date;
    endFreeTime: Date;
  }[];
}

export interface ExpertProfile {
  certImage: string;
  description: string;
}
