import { ReportStatus } from '@prisma/client';

export interface CreateDailyDietReport {
  score: number;
  status: ReportStatus;
  feedback: string;
  dayId: number;
  expertId: number;
}

export interface UpdateDailyDietReport {
  score: number;
  status: ReportStatus;
  feedback: string;
}
