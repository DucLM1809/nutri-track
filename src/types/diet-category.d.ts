export interface CreateDietCategory {
  name: string;
  description: string;
  high: string[];
  low: string[];
}
