export interface CreateCalRecord {
  nutrient: string;
  amount: number;
  unit: string;
  dayCalId: number;
}

export interface UpdateCalRecord {
  nutrient: string;
  amount: number;
  unit: string;
}
