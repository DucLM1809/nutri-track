export interface CreateDailyCal {
  date: Date;
  description?: string;
}
