import { MealType } from '@prisma/client';

export interface CreateMeal {
  name?: string;
  description?: string;
  time: Date;
  type: MealType;
  dayId: number;
  foods: {
    id: number;
    amount: number;
    unit: string;
  }[];
}
