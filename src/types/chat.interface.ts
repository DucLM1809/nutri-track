export enum ChatEvent {
  JOIN_ROOM = 'join-room',
  CHAT_HISTORY = 'chat-history',
  GET_MESSAGES = 'get-messages',
  SEND_MESSAGE = 'send-message'
}
