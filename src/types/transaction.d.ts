export interface CreateTransaction {
  amount: number;
  description: string;
  walletId: number;
}
