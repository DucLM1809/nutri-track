export interface CreateDailyDiet {
  name?: string;
  description?: string;
  date: Date;
  dietId: number;
}
export interface UpdateDailyDiet {
  name?: string;
  description?: string;
  date: Date;
}
