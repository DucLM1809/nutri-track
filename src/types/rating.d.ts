export interface CreateRating {
  score: number;
  description: string;
  expertId: number;
}

export interface UpdateRating {
  score: number;
  description: string;
}
