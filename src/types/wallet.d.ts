export interface CreateWallet {
  balance: number;
  previousBalance: number;
  lastDeposit?: string;
}
