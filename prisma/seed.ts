import { AccountType, Gender, Role } from '@prisma/client';
import prisma from '../src/client';
import { encryptPassword } from '../src/utils/encryption';

async function main() {
  const existingAdmin = await prisma.user.findFirst({ where: { role: Role.ADMIN } });

  if (!existingAdmin) {
    try {
      await prisma.user.create({
        data: {
          email: 'admin@gmail.com',
          name: 'admin',
          avatar: 'test',
          dob: new Date().toISOString(),
          password: await encryptPassword('Aqswde123@'),
          height: 180,
          accountType: AccountType.ADVANCE,
          gender: Gender.MALE,
          role: Role.ADMIN
        }
      });
    } catch (error) {}

    return;
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
